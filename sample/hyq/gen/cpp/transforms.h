#ifndef HYQ_TRANSFORMS_H_
#define HYQ_TRANSFORMS_H_

#include <iit/rbd/TransformsBase.h>
#include "declarations.h"
#include "model_constants.h"
#include "kinematics_parameters.h"

namespace HyQ {
namespace rcg {

struct Parameters
{
    struct AngleFuncValues {
        AngleFuncValues() {
            update();
        }

        void update()
        {
        }
    };

    Params_lengths lengths;
    Params_angles angles;
    AngleFuncValues trig = AngleFuncValues();
};

// The type of the "vector" with the status of the variables
typedef JointState state_t;

template<class M>
using TransformMotion = iit::rbd::SpatialTransformBase<state_t, M>;

template<class M>
using TransformForce = iit::rbd::SpatialTransformBase<state_t, M>;

template<class M>
using TransformHomogeneous = iit::rbd::HomogeneousTransformBase<state_t, M>;

/**
 * The class for the 6-by-6 coordinates transformation matrices for
 * spatial motion vectors.
 */
class MotionTransforms
{
public:
    class Dummy {};
    typedef TransformMotion<Dummy>::MatrixType MatrixType;

    struct Type_fr_trunk_X_LF_foot : public TransformMotion<Type_fr_trunk_X_LF_foot>
    {
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HAA : public TransformMotion<Type_fr_trunk_X_fr_LF_HAA>
    {
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HFE : public TransformMotion<Type_fr_trunk_X_fr_LF_HFE>
    {
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_KFE : public TransformMotion<Type_fr_trunk_X_fr_LF_KFE>
    {
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RF_foot : public TransformMotion<Type_fr_trunk_X_RF_foot>
    {
        Type_fr_trunk_X_RF_foot();
        const Type_fr_trunk_X_RF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HAA : public TransformMotion<Type_fr_trunk_X_fr_RF_HAA>
    {
        Type_fr_trunk_X_fr_RF_HAA();
        const Type_fr_trunk_X_fr_RF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HFE : public TransformMotion<Type_fr_trunk_X_fr_RF_HFE>
    {
        Type_fr_trunk_X_fr_RF_HFE();
        const Type_fr_trunk_X_fr_RF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_KFE : public TransformMotion<Type_fr_trunk_X_fr_RF_KFE>
    {
        Type_fr_trunk_X_fr_RF_KFE();
        const Type_fr_trunk_X_fr_RF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_LH_foot : public TransformMotion<Type_fr_trunk_X_LH_foot>
    {
        Type_fr_trunk_X_LH_foot();
        const Type_fr_trunk_X_LH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HAA : public TransformMotion<Type_fr_trunk_X_fr_LH_HAA>
    {
        Type_fr_trunk_X_fr_LH_HAA();
        const Type_fr_trunk_X_fr_LH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HFE : public TransformMotion<Type_fr_trunk_X_fr_LH_HFE>
    {
        Type_fr_trunk_X_fr_LH_HFE();
        const Type_fr_trunk_X_fr_LH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_KFE : public TransformMotion<Type_fr_trunk_X_fr_LH_KFE>
    {
        Type_fr_trunk_X_fr_LH_KFE();
        const Type_fr_trunk_X_fr_LH_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RH_foot : public TransformMotion<Type_fr_trunk_X_RH_foot>
    {
        Type_fr_trunk_X_RH_foot();
        const Type_fr_trunk_X_RH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HAA : public TransformMotion<Type_fr_trunk_X_fr_RH_HAA>
    {
        Type_fr_trunk_X_fr_RH_HAA();
        const Type_fr_trunk_X_fr_RH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HFE : public TransformMotion<Type_fr_trunk_X_fr_RH_HFE>
    {
        Type_fr_trunk_X_fr_RH_HFE();
        const Type_fr_trunk_X_fr_RH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_KFE : public TransformMotion<Type_fr_trunk_X_fr_RH_KFE>
    {
        Type_fr_trunk_X_fr_RH_KFE();
        const Type_fr_trunk_X_fr_RH_KFE& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_trunk : public TransformMotion<Type_fr_LF_hipassembly_X_fr_trunk>
    {
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_hipassembly : public TransformMotion<Type_fr_trunk_X_fr_LF_hipassembly>
    {
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformMotion<Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformMotion<Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformMotion<Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformMotion<Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_trunk : public TransformMotion<Type_fr_RF_hipassembly_X_fr_trunk>
    {
        Type_fr_RF_hipassembly_X_fr_trunk();
        const Type_fr_RF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_hipassembly : public TransformMotion<Type_fr_trunk_X_fr_RF_hipassembly>
    {
        Type_fr_trunk_X_fr_RF_hipassembly();
        const Type_fr_trunk_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_hipassembly : public TransformMotion<Type_fr_RF_upperleg_X_fr_RF_hipassembly>
    {
        Type_fr_RF_upperleg_X_fr_RF_hipassembly();
        const Type_fr_RF_upperleg_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_RF_upperleg : public TransformMotion<Type_fr_RF_hipassembly_X_fr_RF_upperleg>
    {
        Type_fr_RF_hipassembly_X_fr_RF_upperleg();
        const Type_fr_RF_hipassembly_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_lowerleg_X_fr_RF_upperleg : public TransformMotion<Type_fr_RF_lowerleg_X_fr_RF_upperleg>
    {
        Type_fr_RF_lowerleg_X_fr_RF_upperleg();
        const Type_fr_RF_lowerleg_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_lowerleg : public TransformMotion<Type_fr_RF_upperleg_X_fr_RF_lowerleg>
    {
        Type_fr_RF_upperleg_X_fr_RF_lowerleg();
        const Type_fr_RF_upperleg_X_fr_RF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_trunk : public TransformMotion<Type_fr_LH_hipassembly_X_fr_trunk>
    {
        Type_fr_LH_hipassembly_X_fr_trunk();
        const Type_fr_LH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_hipassembly : public TransformMotion<Type_fr_trunk_X_fr_LH_hipassembly>
    {
        Type_fr_trunk_X_fr_LH_hipassembly();
        const Type_fr_trunk_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_hipassembly : public TransformMotion<Type_fr_LH_upperleg_X_fr_LH_hipassembly>
    {
        Type_fr_LH_upperleg_X_fr_LH_hipassembly();
        const Type_fr_LH_upperleg_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_LH_upperleg : public TransformMotion<Type_fr_LH_hipassembly_X_fr_LH_upperleg>
    {
        Type_fr_LH_hipassembly_X_fr_LH_upperleg();
        const Type_fr_LH_hipassembly_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_lowerleg_X_fr_LH_upperleg : public TransformMotion<Type_fr_LH_lowerleg_X_fr_LH_upperleg>
    {
        Type_fr_LH_lowerleg_X_fr_LH_upperleg();
        const Type_fr_LH_lowerleg_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_lowerleg : public TransformMotion<Type_fr_LH_upperleg_X_fr_LH_lowerleg>
    {
        Type_fr_LH_upperleg_X_fr_LH_lowerleg();
        const Type_fr_LH_upperleg_X_fr_LH_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_trunk : public TransformMotion<Type_fr_RH_hipassembly_X_fr_trunk>
    {
        Type_fr_RH_hipassembly_X_fr_trunk();
        const Type_fr_RH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_hipassembly : public TransformMotion<Type_fr_trunk_X_fr_RH_hipassembly>
    {
        Type_fr_trunk_X_fr_RH_hipassembly();
        const Type_fr_trunk_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_hipassembly : public TransformMotion<Type_fr_RH_upperleg_X_fr_RH_hipassembly>
    {
        Type_fr_RH_upperleg_X_fr_RH_hipassembly();
        const Type_fr_RH_upperleg_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_RH_upperleg : public TransformMotion<Type_fr_RH_hipassembly_X_fr_RH_upperleg>
    {
        Type_fr_RH_hipassembly_X_fr_RH_upperleg();
        const Type_fr_RH_hipassembly_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_lowerleg_X_fr_RH_upperleg : public TransformMotion<Type_fr_RH_lowerleg_X_fr_RH_upperleg>
    {
        Type_fr_RH_lowerleg_X_fr_RH_upperleg();
        const Type_fr_RH_lowerleg_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_lowerleg : public TransformMotion<Type_fr_RH_upperleg_X_fr_RH_lowerleg>
    {
        Type_fr_RH_upperleg_X_fr_RH_lowerleg();
        const Type_fr_RH_upperleg_X_fr_RH_lowerleg& update(const state_t&);
    };
    
public:
    MotionTransforms();
    void updateParams(const Params_lengths&, const Params_angles&);

    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_trunk_X_RF_foot fr_trunk_X_RF_foot;
    Type_fr_trunk_X_fr_RF_HAA fr_trunk_X_fr_RF_HAA;
    Type_fr_trunk_X_fr_RF_HFE fr_trunk_X_fr_RF_HFE;
    Type_fr_trunk_X_fr_RF_KFE fr_trunk_X_fr_RF_KFE;
    Type_fr_trunk_X_LH_foot fr_trunk_X_LH_foot;
    Type_fr_trunk_X_fr_LH_HAA fr_trunk_X_fr_LH_HAA;
    Type_fr_trunk_X_fr_LH_HFE fr_trunk_X_fr_LH_HFE;
    Type_fr_trunk_X_fr_LH_KFE fr_trunk_X_fr_LH_KFE;
    Type_fr_trunk_X_RH_foot fr_trunk_X_RH_foot;
    Type_fr_trunk_X_fr_RH_HAA fr_trunk_X_fr_RH_HAA;
    Type_fr_trunk_X_fr_RH_HFE fr_trunk_X_fr_RH_HFE;
    Type_fr_trunk_X_fr_RH_KFE fr_trunk_X_fr_RH_KFE;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;
    Type_fr_RF_hipassembly_X_fr_trunk fr_RF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RF_hipassembly fr_trunk_X_fr_RF_hipassembly;
    Type_fr_RF_upperleg_X_fr_RF_hipassembly fr_RF_upperleg_X_fr_RF_hipassembly;
    Type_fr_RF_hipassembly_X_fr_RF_upperleg fr_RF_hipassembly_X_fr_RF_upperleg;
    Type_fr_RF_lowerleg_X_fr_RF_upperleg fr_RF_lowerleg_X_fr_RF_upperleg;
    Type_fr_RF_upperleg_X_fr_RF_lowerleg fr_RF_upperleg_X_fr_RF_lowerleg;
    Type_fr_LH_hipassembly_X_fr_trunk fr_LH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LH_hipassembly fr_trunk_X_fr_LH_hipassembly;
    Type_fr_LH_upperleg_X_fr_LH_hipassembly fr_LH_upperleg_X_fr_LH_hipassembly;
    Type_fr_LH_hipassembly_X_fr_LH_upperleg fr_LH_hipassembly_X_fr_LH_upperleg;
    Type_fr_LH_lowerleg_X_fr_LH_upperleg fr_LH_lowerleg_X_fr_LH_upperleg;
    Type_fr_LH_upperleg_X_fr_LH_lowerleg fr_LH_upperleg_X_fr_LH_lowerleg;
    Type_fr_RH_hipassembly_X_fr_trunk fr_RH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RH_hipassembly fr_trunk_X_fr_RH_hipassembly;
    Type_fr_RH_upperleg_X_fr_RH_hipassembly fr_RH_upperleg_X_fr_RH_hipassembly;
    Type_fr_RH_hipassembly_X_fr_RH_upperleg fr_RH_hipassembly_X_fr_RH_upperleg;
    Type_fr_RH_lowerleg_X_fr_RH_upperleg fr_RH_lowerleg_X_fr_RH_upperleg;
    Type_fr_RH_upperleg_X_fr_RH_lowerleg fr_RH_upperleg_X_fr_RH_lowerleg;

protected:
    Parameters params;

}; //class 'MotionTransforms'

/**
 * The class for the 6-by-6 coordinates transformation matrices for
 * spatial force vectors.
 */
class ForceTransforms
{
public:
    class Dummy {};
    typedef TransformForce<Dummy>::MatrixType MatrixType;

    struct Type_fr_trunk_X_LF_foot : public TransformForce<Type_fr_trunk_X_LF_foot>
    {
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HAA : public TransformForce<Type_fr_trunk_X_fr_LF_HAA>
    {
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HFE : public TransformForce<Type_fr_trunk_X_fr_LF_HFE>
    {
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_KFE : public TransformForce<Type_fr_trunk_X_fr_LF_KFE>
    {
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RF_foot : public TransformForce<Type_fr_trunk_X_RF_foot>
    {
        Type_fr_trunk_X_RF_foot();
        const Type_fr_trunk_X_RF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HAA : public TransformForce<Type_fr_trunk_X_fr_RF_HAA>
    {
        Type_fr_trunk_X_fr_RF_HAA();
        const Type_fr_trunk_X_fr_RF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HFE : public TransformForce<Type_fr_trunk_X_fr_RF_HFE>
    {
        Type_fr_trunk_X_fr_RF_HFE();
        const Type_fr_trunk_X_fr_RF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_KFE : public TransformForce<Type_fr_trunk_X_fr_RF_KFE>
    {
        Type_fr_trunk_X_fr_RF_KFE();
        const Type_fr_trunk_X_fr_RF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_LH_foot : public TransformForce<Type_fr_trunk_X_LH_foot>
    {
        Type_fr_trunk_X_LH_foot();
        const Type_fr_trunk_X_LH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HAA : public TransformForce<Type_fr_trunk_X_fr_LH_HAA>
    {
        Type_fr_trunk_X_fr_LH_HAA();
        const Type_fr_trunk_X_fr_LH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HFE : public TransformForce<Type_fr_trunk_X_fr_LH_HFE>
    {
        Type_fr_trunk_X_fr_LH_HFE();
        const Type_fr_trunk_X_fr_LH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_KFE : public TransformForce<Type_fr_trunk_X_fr_LH_KFE>
    {
        Type_fr_trunk_X_fr_LH_KFE();
        const Type_fr_trunk_X_fr_LH_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RH_foot : public TransformForce<Type_fr_trunk_X_RH_foot>
    {
        Type_fr_trunk_X_RH_foot();
        const Type_fr_trunk_X_RH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HAA : public TransformForce<Type_fr_trunk_X_fr_RH_HAA>
    {
        Type_fr_trunk_X_fr_RH_HAA();
        const Type_fr_trunk_X_fr_RH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HFE : public TransformForce<Type_fr_trunk_X_fr_RH_HFE>
    {
        Type_fr_trunk_X_fr_RH_HFE();
        const Type_fr_trunk_X_fr_RH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_KFE : public TransformForce<Type_fr_trunk_X_fr_RH_KFE>
    {
        Type_fr_trunk_X_fr_RH_KFE();
        const Type_fr_trunk_X_fr_RH_KFE& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_trunk : public TransformForce<Type_fr_LF_hipassembly_X_fr_trunk>
    {
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_hipassembly : public TransformForce<Type_fr_trunk_X_fr_LF_hipassembly>
    {
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformForce<Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformForce<Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformForce<Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformForce<Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_trunk : public TransformForce<Type_fr_RF_hipassembly_X_fr_trunk>
    {
        Type_fr_RF_hipassembly_X_fr_trunk();
        const Type_fr_RF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_hipassembly : public TransformForce<Type_fr_trunk_X_fr_RF_hipassembly>
    {
        Type_fr_trunk_X_fr_RF_hipassembly();
        const Type_fr_trunk_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_hipassembly : public TransformForce<Type_fr_RF_upperleg_X_fr_RF_hipassembly>
    {
        Type_fr_RF_upperleg_X_fr_RF_hipassembly();
        const Type_fr_RF_upperleg_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_RF_upperleg : public TransformForce<Type_fr_RF_hipassembly_X_fr_RF_upperleg>
    {
        Type_fr_RF_hipassembly_X_fr_RF_upperleg();
        const Type_fr_RF_hipassembly_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_lowerleg_X_fr_RF_upperleg : public TransformForce<Type_fr_RF_lowerleg_X_fr_RF_upperleg>
    {
        Type_fr_RF_lowerleg_X_fr_RF_upperleg();
        const Type_fr_RF_lowerleg_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_lowerleg : public TransformForce<Type_fr_RF_upperleg_X_fr_RF_lowerleg>
    {
        Type_fr_RF_upperleg_X_fr_RF_lowerleg();
        const Type_fr_RF_upperleg_X_fr_RF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_trunk : public TransformForce<Type_fr_LH_hipassembly_X_fr_trunk>
    {
        Type_fr_LH_hipassembly_X_fr_trunk();
        const Type_fr_LH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_hipassembly : public TransformForce<Type_fr_trunk_X_fr_LH_hipassembly>
    {
        Type_fr_trunk_X_fr_LH_hipassembly();
        const Type_fr_trunk_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_hipassembly : public TransformForce<Type_fr_LH_upperleg_X_fr_LH_hipassembly>
    {
        Type_fr_LH_upperleg_X_fr_LH_hipassembly();
        const Type_fr_LH_upperleg_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_LH_upperleg : public TransformForce<Type_fr_LH_hipassembly_X_fr_LH_upperleg>
    {
        Type_fr_LH_hipassembly_X_fr_LH_upperleg();
        const Type_fr_LH_hipassembly_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_lowerleg_X_fr_LH_upperleg : public TransformForce<Type_fr_LH_lowerleg_X_fr_LH_upperleg>
    {
        Type_fr_LH_lowerleg_X_fr_LH_upperleg();
        const Type_fr_LH_lowerleg_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_lowerleg : public TransformForce<Type_fr_LH_upperleg_X_fr_LH_lowerleg>
    {
        Type_fr_LH_upperleg_X_fr_LH_lowerleg();
        const Type_fr_LH_upperleg_X_fr_LH_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_trunk : public TransformForce<Type_fr_RH_hipassembly_X_fr_trunk>
    {
        Type_fr_RH_hipassembly_X_fr_trunk();
        const Type_fr_RH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_hipassembly : public TransformForce<Type_fr_trunk_X_fr_RH_hipassembly>
    {
        Type_fr_trunk_X_fr_RH_hipassembly();
        const Type_fr_trunk_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_hipassembly : public TransformForce<Type_fr_RH_upperleg_X_fr_RH_hipassembly>
    {
        Type_fr_RH_upperleg_X_fr_RH_hipassembly();
        const Type_fr_RH_upperleg_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_RH_upperleg : public TransformForce<Type_fr_RH_hipassembly_X_fr_RH_upperleg>
    {
        Type_fr_RH_hipassembly_X_fr_RH_upperleg();
        const Type_fr_RH_hipassembly_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_lowerleg_X_fr_RH_upperleg : public TransformForce<Type_fr_RH_lowerleg_X_fr_RH_upperleg>
    {
        Type_fr_RH_lowerleg_X_fr_RH_upperleg();
        const Type_fr_RH_lowerleg_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_lowerleg : public TransformForce<Type_fr_RH_upperleg_X_fr_RH_lowerleg>
    {
        Type_fr_RH_upperleg_X_fr_RH_lowerleg();
        const Type_fr_RH_upperleg_X_fr_RH_lowerleg& update(const state_t&);
    };
    
public:
    ForceTransforms();
    void updateParams(const Params_lengths&, const Params_angles&);

    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_trunk_X_RF_foot fr_trunk_X_RF_foot;
    Type_fr_trunk_X_fr_RF_HAA fr_trunk_X_fr_RF_HAA;
    Type_fr_trunk_X_fr_RF_HFE fr_trunk_X_fr_RF_HFE;
    Type_fr_trunk_X_fr_RF_KFE fr_trunk_X_fr_RF_KFE;
    Type_fr_trunk_X_LH_foot fr_trunk_X_LH_foot;
    Type_fr_trunk_X_fr_LH_HAA fr_trunk_X_fr_LH_HAA;
    Type_fr_trunk_X_fr_LH_HFE fr_trunk_X_fr_LH_HFE;
    Type_fr_trunk_X_fr_LH_KFE fr_trunk_X_fr_LH_KFE;
    Type_fr_trunk_X_RH_foot fr_trunk_X_RH_foot;
    Type_fr_trunk_X_fr_RH_HAA fr_trunk_X_fr_RH_HAA;
    Type_fr_trunk_X_fr_RH_HFE fr_trunk_X_fr_RH_HFE;
    Type_fr_trunk_X_fr_RH_KFE fr_trunk_X_fr_RH_KFE;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;
    Type_fr_RF_hipassembly_X_fr_trunk fr_RF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RF_hipassembly fr_trunk_X_fr_RF_hipassembly;
    Type_fr_RF_upperleg_X_fr_RF_hipassembly fr_RF_upperleg_X_fr_RF_hipassembly;
    Type_fr_RF_hipassembly_X_fr_RF_upperleg fr_RF_hipassembly_X_fr_RF_upperleg;
    Type_fr_RF_lowerleg_X_fr_RF_upperleg fr_RF_lowerleg_X_fr_RF_upperleg;
    Type_fr_RF_upperleg_X_fr_RF_lowerleg fr_RF_upperleg_X_fr_RF_lowerleg;
    Type_fr_LH_hipassembly_X_fr_trunk fr_LH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LH_hipassembly fr_trunk_X_fr_LH_hipassembly;
    Type_fr_LH_upperleg_X_fr_LH_hipassembly fr_LH_upperleg_X_fr_LH_hipassembly;
    Type_fr_LH_hipassembly_X_fr_LH_upperleg fr_LH_hipassembly_X_fr_LH_upperleg;
    Type_fr_LH_lowerleg_X_fr_LH_upperleg fr_LH_lowerleg_X_fr_LH_upperleg;
    Type_fr_LH_upperleg_X_fr_LH_lowerleg fr_LH_upperleg_X_fr_LH_lowerleg;
    Type_fr_RH_hipassembly_X_fr_trunk fr_RH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RH_hipassembly fr_trunk_X_fr_RH_hipassembly;
    Type_fr_RH_upperleg_X_fr_RH_hipassembly fr_RH_upperleg_X_fr_RH_hipassembly;
    Type_fr_RH_hipassembly_X_fr_RH_upperleg fr_RH_hipassembly_X_fr_RH_upperleg;
    Type_fr_RH_lowerleg_X_fr_RH_upperleg fr_RH_lowerleg_X_fr_RH_upperleg;
    Type_fr_RH_upperleg_X_fr_RH_lowerleg fr_RH_upperleg_X_fr_RH_lowerleg;

protected:
    Parameters params;

}; //class 'ForceTransforms'

/**
 * The class with the homogeneous (4x4) coordinates transformation
 * matrices.
 */
class HomogeneousTransforms
{
public:
    class Dummy {};
    typedef TransformHomogeneous<Dummy>::MatrixType MatrixType;

    struct Type_fr_trunk_X_LF_foot : public TransformHomogeneous<Type_fr_trunk_X_LF_foot>
    {
        Type_fr_trunk_X_LF_foot();
        const Type_fr_trunk_X_LF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HAA : public TransformHomogeneous<Type_fr_trunk_X_fr_LF_HAA>
    {
        Type_fr_trunk_X_fr_LF_HAA();
        const Type_fr_trunk_X_fr_LF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_HFE : public TransformHomogeneous<Type_fr_trunk_X_fr_LF_HFE>
    {
        Type_fr_trunk_X_fr_LF_HFE();
        const Type_fr_trunk_X_fr_LF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_KFE : public TransformHomogeneous<Type_fr_trunk_X_fr_LF_KFE>
    {
        Type_fr_trunk_X_fr_LF_KFE();
        const Type_fr_trunk_X_fr_LF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RF_foot : public TransformHomogeneous<Type_fr_trunk_X_RF_foot>
    {
        Type_fr_trunk_X_RF_foot();
        const Type_fr_trunk_X_RF_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HAA : public TransformHomogeneous<Type_fr_trunk_X_fr_RF_HAA>
    {
        Type_fr_trunk_X_fr_RF_HAA();
        const Type_fr_trunk_X_fr_RF_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_HFE : public TransformHomogeneous<Type_fr_trunk_X_fr_RF_HFE>
    {
        Type_fr_trunk_X_fr_RF_HFE();
        const Type_fr_trunk_X_fr_RF_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_KFE : public TransformHomogeneous<Type_fr_trunk_X_fr_RF_KFE>
    {
        Type_fr_trunk_X_fr_RF_KFE();
        const Type_fr_trunk_X_fr_RF_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_LH_foot : public TransformHomogeneous<Type_fr_trunk_X_LH_foot>
    {
        Type_fr_trunk_X_LH_foot();
        const Type_fr_trunk_X_LH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HAA : public TransformHomogeneous<Type_fr_trunk_X_fr_LH_HAA>
    {
        Type_fr_trunk_X_fr_LH_HAA();
        const Type_fr_trunk_X_fr_LH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_HFE : public TransformHomogeneous<Type_fr_trunk_X_fr_LH_HFE>
    {
        Type_fr_trunk_X_fr_LH_HFE();
        const Type_fr_trunk_X_fr_LH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_KFE : public TransformHomogeneous<Type_fr_trunk_X_fr_LH_KFE>
    {
        Type_fr_trunk_X_fr_LH_KFE();
        const Type_fr_trunk_X_fr_LH_KFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_RH_foot : public TransformHomogeneous<Type_fr_trunk_X_RH_foot>
    {
        Type_fr_trunk_X_RH_foot();
        const Type_fr_trunk_X_RH_foot& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HAA : public TransformHomogeneous<Type_fr_trunk_X_fr_RH_HAA>
    {
        Type_fr_trunk_X_fr_RH_HAA();
        const Type_fr_trunk_X_fr_RH_HAA& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_HFE : public TransformHomogeneous<Type_fr_trunk_X_fr_RH_HFE>
    {
        Type_fr_trunk_X_fr_RH_HFE();
        const Type_fr_trunk_X_fr_RH_HFE& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_KFE : public TransformHomogeneous<Type_fr_trunk_X_fr_RH_KFE>
    {
        Type_fr_trunk_X_fr_RH_KFE();
        const Type_fr_trunk_X_fr_RH_KFE& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_trunk : public TransformHomogeneous<Type_fr_LF_hipassembly_X_fr_trunk>
    {
        Type_fr_LF_hipassembly_X_fr_trunk();
        const Type_fr_LF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LF_hipassembly : public TransformHomogeneous<Type_fr_trunk_X_fr_LF_hipassembly>
    {
        Type_fr_trunk_X_fr_LF_hipassembly();
        const Type_fr_trunk_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_hipassembly : public TransformHomogeneous<Type_fr_LF_upperleg_X_fr_LF_hipassembly>
    {
        Type_fr_LF_upperleg_X_fr_LF_hipassembly();
        const Type_fr_LF_upperleg_X_fr_LF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LF_hipassembly_X_fr_LF_upperleg : public TransformHomogeneous<Type_fr_LF_hipassembly_X_fr_LF_upperleg>
    {
        Type_fr_LF_hipassembly_X_fr_LF_upperleg();
        const Type_fr_LF_hipassembly_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_lowerleg_X_fr_LF_upperleg : public TransformHomogeneous<Type_fr_LF_lowerleg_X_fr_LF_upperleg>
    {
        Type_fr_LF_lowerleg_X_fr_LF_upperleg();
        const Type_fr_LF_lowerleg_X_fr_LF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LF_upperleg_X_fr_LF_lowerleg : public TransformHomogeneous<Type_fr_LF_upperleg_X_fr_LF_lowerleg>
    {
        Type_fr_LF_upperleg_X_fr_LF_lowerleg();
        const Type_fr_LF_upperleg_X_fr_LF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_trunk : public TransformHomogeneous<Type_fr_RF_hipassembly_X_fr_trunk>
    {
        Type_fr_RF_hipassembly_X_fr_trunk();
        const Type_fr_RF_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RF_hipassembly : public TransformHomogeneous<Type_fr_trunk_X_fr_RF_hipassembly>
    {
        Type_fr_trunk_X_fr_RF_hipassembly();
        const Type_fr_trunk_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_hipassembly : public TransformHomogeneous<Type_fr_RF_upperleg_X_fr_RF_hipassembly>
    {
        Type_fr_RF_upperleg_X_fr_RF_hipassembly();
        const Type_fr_RF_upperleg_X_fr_RF_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RF_hipassembly_X_fr_RF_upperleg : public TransformHomogeneous<Type_fr_RF_hipassembly_X_fr_RF_upperleg>
    {
        Type_fr_RF_hipassembly_X_fr_RF_upperleg();
        const Type_fr_RF_hipassembly_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_lowerleg_X_fr_RF_upperleg : public TransformHomogeneous<Type_fr_RF_lowerleg_X_fr_RF_upperleg>
    {
        Type_fr_RF_lowerleg_X_fr_RF_upperleg();
        const Type_fr_RF_lowerleg_X_fr_RF_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RF_upperleg_X_fr_RF_lowerleg : public TransformHomogeneous<Type_fr_RF_upperleg_X_fr_RF_lowerleg>
    {
        Type_fr_RF_upperleg_X_fr_RF_lowerleg();
        const Type_fr_RF_upperleg_X_fr_RF_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_trunk : public TransformHomogeneous<Type_fr_LH_hipassembly_X_fr_trunk>
    {
        Type_fr_LH_hipassembly_X_fr_trunk();
        const Type_fr_LH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_LH_hipassembly : public TransformHomogeneous<Type_fr_trunk_X_fr_LH_hipassembly>
    {
        Type_fr_trunk_X_fr_LH_hipassembly();
        const Type_fr_trunk_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_hipassembly : public TransformHomogeneous<Type_fr_LH_upperleg_X_fr_LH_hipassembly>
    {
        Type_fr_LH_upperleg_X_fr_LH_hipassembly();
        const Type_fr_LH_upperleg_X_fr_LH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_LH_hipassembly_X_fr_LH_upperleg : public TransformHomogeneous<Type_fr_LH_hipassembly_X_fr_LH_upperleg>
    {
        Type_fr_LH_hipassembly_X_fr_LH_upperleg();
        const Type_fr_LH_hipassembly_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_lowerleg_X_fr_LH_upperleg : public TransformHomogeneous<Type_fr_LH_lowerleg_X_fr_LH_upperleg>
    {
        Type_fr_LH_lowerleg_X_fr_LH_upperleg();
        const Type_fr_LH_lowerleg_X_fr_LH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_LH_upperleg_X_fr_LH_lowerleg : public TransformHomogeneous<Type_fr_LH_upperleg_X_fr_LH_lowerleg>
    {
        Type_fr_LH_upperleg_X_fr_LH_lowerleg();
        const Type_fr_LH_upperleg_X_fr_LH_lowerleg& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_trunk : public TransformHomogeneous<Type_fr_RH_hipassembly_X_fr_trunk>
    {
        Type_fr_RH_hipassembly_X_fr_trunk();
        const Type_fr_RH_hipassembly_X_fr_trunk& update(const state_t&);
    };
    
    struct Type_fr_trunk_X_fr_RH_hipassembly : public TransformHomogeneous<Type_fr_trunk_X_fr_RH_hipassembly>
    {
        Type_fr_trunk_X_fr_RH_hipassembly();
        const Type_fr_trunk_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_hipassembly : public TransformHomogeneous<Type_fr_RH_upperleg_X_fr_RH_hipassembly>
    {
        Type_fr_RH_upperleg_X_fr_RH_hipassembly();
        const Type_fr_RH_upperleg_X_fr_RH_hipassembly& update(const state_t&);
    };
    
    struct Type_fr_RH_hipassembly_X_fr_RH_upperleg : public TransformHomogeneous<Type_fr_RH_hipassembly_X_fr_RH_upperleg>
    {
        Type_fr_RH_hipassembly_X_fr_RH_upperleg();
        const Type_fr_RH_hipassembly_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_lowerleg_X_fr_RH_upperleg : public TransformHomogeneous<Type_fr_RH_lowerleg_X_fr_RH_upperleg>
    {
        Type_fr_RH_lowerleg_X_fr_RH_upperleg();
        const Type_fr_RH_lowerleg_X_fr_RH_upperleg& update(const state_t&);
    };
    
    struct Type_fr_RH_upperleg_X_fr_RH_lowerleg : public TransformHomogeneous<Type_fr_RH_upperleg_X_fr_RH_lowerleg>
    {
        Type_fr_RH_upperleg_X_fr_RH_lowerleg();
        const Type_fr_RH_upperleg_X_fr_RH_lowerleg& update(const state_t&);
    };
    
public:
    HomogeneousTransforms();
    void updateParams(const Params_lengths&, const Params_angles&);

    Type_fr_trunk_X_LF_foot fr_trunk_X_LF_foot;
    Type_fr_trunk_X_fr_LF_HAA fr_trunk_X_fr_LF_HAA;
    Type_fr_trunk_X_fr_LF_HFE fr_trunk_X_fr_LF_HFE;
    Type_fr_trunk_X_fr_LF_KFE fr_trunk_X_fr_LF_KFE;
    Type_fr_trunk_X_RF_foot fr_trunk_X_RF_foot;
    Type_fr_trunk_X_fr_RF_HAA fr_trunk_X_fr_RF_HAA;
    Type_fr_trunk_X_fr_RF_HFE fr_trunk_X_fr_RF_HFE;
    Type_fr_trunk_X_fr_RF_KFE fr_trunk_X_fr_RF_KFE;
    Type_fr_trunk_X_LH_foot fr_trunk_X_LH_foot;
    Type_fr_trunk_X_fr_LH_HAA fr_trunk_X_fr_LH_HAA;
    Type_fr_trunk_X_fr_LH_HFE fr_trunk_X_fr_LH_HFE;
    Type_fr_trunk_X_fr_LH_KFE fr_trunk_X_fr_LH_KFE;
    Type_fr_trunk_X_RH_foot fr_trunk_X_RH_foot;
    Type_fr_trunk_X_fr_RH_HAA fr_trunk_X_fr_RH_HAA;
    Type_fr_trunk_X_fr_RH_HFE fr_trunk_X_fr_RH_HFE;
    Type_fr_trunk_X_fr_RH_KFE fr_trunk_X_fr_RH_KFE;
    Type_fr_LF_hipassembly_X_fr_trunk fr_LF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LF_hipassembly fr_trunk_X_fr_LF_hipassembly;
    Type_fr_LF_upperleg_X_fr_LF_hipassembly fr_LF_upperleg_X_fr_LF_hipassembly;
    Type_fr_LF_hipassembly_X_fr_LF_upperleg fr_LF_hipassembly_X_fr_LF_upperleg;
    Type_fr_LF_lowerleg_X_fr_LF_upperleg fr_LF_lowerleg_X_fr_LF_upperleg;
    Type_fr_LF_upperleg_X_fr_LF_lowerleg fr_LF_upperleg_X_fr_LF_lowerleg;
    Type_fr_RF_hipassembly_X_fr_trunk fr_RF_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RF_hipassembly fr_trunk_X_fr_RF_hipassembly;
    Type_fr_RF_upperleg_X_fr_RF_hipassembly fr_RF_upperleg_X_fr_RF_hipassembly;
    Type_fr_RF_hipassembly_X_fr_RF_upperleg fr_RF_hipassembly_X_fr_RF_upperleg;
    Type_fr_RF_lowerleg_X_fr_RF_upperleg fr_RF_lowerleg_X_fr_RF_upperleg;
    Type_fr_RF_upperleg_X_fr_RF_lowerleg fr_RF_upperleg_X_fr_RF_lowerleg;
    Type_fr_LH_hipassembly_X_fr_trunk fr_LH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_LH_hipassembly fr_trunk_X_fr_LH_hipassembly;
    Type_fr_LH_upperleg_X_fr_LH_hipassembly fr_LH_upperleg_X_fr_LH_hipassembly;
    Type_fr_LH_hipassembly_X_fr_LH_upperleg fr_LH_hipassembly_X_fr_LH_upperleg;
    Type_fr_LH_lowerleg_X_fr_LH_upperleg fr_LH_lowerleg_X_fr_LH_upperleg;
    Type_fr_LH_upperleg_X_fr_LH_lowerleg fr_LH_upperleg_X_fr_LH_lowerleg;
    Type_fr_RH_hipassembly_X_fr_trunk fr_RH_hipassembly_X_fr_trunk;
    Type_fr_trunk_X_fr_RH_hipassembly fr_trunk_X_fr_RH_hipassembly;
    Type_fr_RH_upperleg_X_fr_RH_hipassembly fr_RH_upperleg_X_fr_RH_hipassembly;
    Type_fr_RH_hipassembly_X_fr_RH_upperleg fr_RH_hipassembly_X_fr_RH_upperleg;
    Type_fr_RH_lowerleg_X_fr_RH_upperleg fr_RH_lowerleg_X_fr_RH_upperleg;
    Type_fr_RH_upperleg_X_fr_RH_lowerleg fr_RH_upperleg_X_fr_RH_lowerleg;

protected:
    Parameters params;

}; //class 'HomogeneousTransforms'

}
}

#endif
