#ifndef RCG_HYQ_MODEL_CONSTANTS_H_
#define RCG_HYQ_MODEL_CONSTANTS_H_

#include "rbd_types.h"

/**
 * \file
 * This file contains the definitions of all the non-zero numerical
 * constants of the robot model (i.e. the numbers appearing in the
 * .kindsl file).
 *
 * Varying these values (and recompiling) is a quick & dirty
 * way to vary the kinematics/dynamics model. For a much more
 * flexible way of exploring variations of the model, consider
 * using the parametrization feature of RobCoGen (see the wiki).
 *
 * Beware of inconsistencies when changing any of the inertia
 * properties.
 */

namespace HyQ {
namespace rcg {

// Do not use 'constexpr' to allow for non-literal scalar types

const Scalar tx_LF_HAA = 0.3734999895095825;
const Scalar ty_LF_HAA = 0.2070000022649765;
const Scalar tx_LF_HFE = 0.07999999821186066;
const Scalar tx_LF_KFE = 0.3499999940395355;
const Scalar tx_RF_HAA = 0.3734999895095825;
const Scalar ty_RF_HAA = -0.2070000022649765;
const Scalar tx_RF_HFE = 0.07999999821186066;
const Scalar tx_RF_KFE = 0.3499999940395355;
const Scalar tx_LH_HAA = -0.3734999895095825;
const Scalar ty_LH_HAA = 0.2070000022649765;
const Scalar tx_LH_HFE = 0.07999999821186066;
const Scalar tx_LH_KFE = 0.3499999940395355;
const Scalar tx_RH_HAA = -0.3734999895095825;
const Scalar ty_RH_HAA = -0.2070000022649765;
const Scalar tx_RH_HFE = 0.07999999821186066;
const Scalar tx_RH_KFE = 0.3499999940395355;
const Scalar tx_LF_foot = 0.33000001311302185;
const Scalar tx_RF_foot = 0.33000001311302185;
const Scalar tx_LH_foot = 0.33000001311302185;
const Scalar tx_RH_foot = 0.33000001311302185;
const Scalar m_trunk = 55.400001525878906;
const Scalar comx_trunk = -0.024000000208616257;
const Scalar comy_trunk = 0.002300000051036477;
const Scalar ix_trunk = 1.6882380247116089;
const Scalar ixy_trunk = -0.02878900058567524;
const Scalar ixz_trunk = 0.2776939868927002;
const Scalar iy_trunk = 8.665299415588379;
const Scalar iyz_trunk = 0.003819999983534217;
const Scalar iz_trunk = 9.243559837341309;
const Scalar m_LF_hipassembly = 2.930000066757202;
const Scalar comx_LF_hipassembly = 0.04262999817728996;
const Scalar comz_LF_hipassembly = 0.16931000351905823;
const Scalar ix_LF_hipassembly = 0.13470500707626343;
const Scalar ixy_LF_hipassembly = 3.600000127335079E-5;
const Scalar ixz_LF_hipassembly = 0.02273399941623211;
const Scalar iy_LF_hipassembly = 0.14417099952697754;
const Scalar iyz_LF_hipassembly = 5.0999999075429514E-5;
const Scalar iz_LF_hipassembly = 0.01103300042450428;
const Scalar m_LF_upperleg = 2.638000011444092;
const Scalar comx_LF_upperleg = 0.15073999762535095;
const Scalar comy_LF_upperleg = -0.026249999180436134;
const Scalar ix_LF_upperleg = 0.005495000164955854;
const Scalar ixy_LF_upperleg = -0.007418000139296055;
const Scalar ixz_LF_upperleg = -1.0199999815085903E-4;
const Scalar iy_LF_upperleg = 0.08713600039482117;
const Scalar iyz_LF_upperleg = -2.099999983329326E-5;
const Scalar iz_LF_upperleg = 0.08987099677324295;
const Scalar m_LF_lowerleg = 0.8809999823570251;
const Scalar comx_LF_lowerleg = 0.12540000677108765;
const Scalar comy_LF_lowerleg = 5.000000237487257E-4;
const Scalar comz_LF_lowerleg = -9.999999747378752E-5;
const Scalar ix_LF_lowerleg = 4.6800001291558146E-4;
const Scalar iy_LF_lowerleg = 0.026409000158309937;
const Scalar iz_LF_lowerleg = 0.026180999353528023;
const Scalar m_RF_hipassembly = 2.930000066757202;
const Scalar comx_RF_hipassembly = 0.04262999817728996;
const Scalar comz_RF_hipassembly = -0.16931000351905823;
const Scalar ix_RF_hipassembly = 0.13470500707626343;
const Scalar ixy_RF_hipassembly = -3.600000127335079E-5;
const Scalar ixz_RF_hipassembly = -0.02273399941623211;
const Scalar iy_RF_hipassembly = 0.14417099952697754;
const Scalar iyz_RF_hipassembly = 5.0999999075429514E-5;
const Scalar iz_RF_hipassembly = 0.01103300042450428;
const Scalar m_RF_upperleg = 2.638000011444092;
const Scalar comx_RF_upperleg = 0.15073999762535095;
const Scalar comy_RF_upperleg = -0.026249999180436134;
const Scalar ix_RF_upperleg = 0.005495000164955854;
const Scalar ixy_RF_upperleg = -0.007418000139296055;
const Scalar ixz_RF_upperleg = -1.0199999815085903E-4;
const Scalar iy_RF_upperleg = 0.08713600039482117;
const Scalar iyz_RF_upperleg = -2.099999983329326E-5;
const Scalar iz_RF_upperleg = 0.08987099677324295;
const Scalar m_RF_lowerleg = 0.8809999823570251;
const Scalar comx_RF_lowerleg = 0.12540000677108765;
const Scalar comy_RF_lowerleg = 5.000000237487257E-4;
const Scalar comz_RF_lowerleg = -9.999999747378752E-5;
const Scalar ix_RF_lowerleg = 4.6800001291558146E-4;
const Scalar iy_RF_lowerleg = 0.026409000158309937;
const Scalar iz_RF_lowerleg = 0.026180999353528023;
const Scalar m_LH_hipassembly = 2.930000066757202;
const Scalar comx_LH_hipassembly = 0.04262999817728996;
const Scalar comz_LH_hipassembly = -0.16931000351905823;
const Scalar ix_LH_hipassembly = 0.13470500707626343;
const Scalar ixy_LH_hipassembly = -3.600000127335079E-5;
const Scalar ixz_LH_hipassembly = -0.02273399941623211;
const Scalar iy_LH_hipassembly = 0.14417099952697754;
const Scalar iyz_LH_hipassembly = 5.0999999075429514E-5;
const Scalar iz_LH_hipassembly = 0.01103300042450428;
const Scalar m_LH_upperleg = 2.638000011444092;
const Scalar comx_LH_upperleg = 0.15073999762535095;
const Scalar comy_LH_upperleg = 0.026249999180436134;
const Scalar ix_LH_upperleg = 0.005495000164955854;
const Scalar ixy_LH_upperleg = 0.007418000139296055;
const Scalar ixz_LH_upperleg = 1.0199999815085903E-4;
const Scalar iy_LH_upperleg = 0.08713600039482117;
const Scalar iyz_LH_upperleg = -2.099999983329326E-5;
const Scalar iz_LH_upperleg = 0.08987099677324295;
const Scalar m_LH_lowerleg = 0.8809999823570251;
const Scalar comx_LH_lowerleg = 0.12540000677108765;
const Scalar comy_LH_lowerleg = -5.000000237487257E-4;
const Scalar comz_LH_lowerleg = 9.999999747378752E-5;
const Scalar ix_LH_lowerleg = 4.6800001291558146E-4;
const Scalar iy_LH_lowerleg = 0.026409000158309937;
const Scalar iz_LH_lowerleg = 0.026180999353528023;
const Scalar m_RH_hipassembly = 2.930000066757202;
const Scalar comx_RH_hipassembly = 0.04262999817728996;
const Scalar comz_RH_hipassembly = 0.16931000351905823;
const Scalar ix_RH_hipassembly = 0.13470500707626343;
const Scalar ixy_RH_hipassembly = 3.600000127335079E-5;
const Scalar ixz_RH_hipassembly = 0.02273399941623211;
const Scalar iy_RH_hipassembly = 0.14417099952697754;
const Scalar iyz_RH_hipassembly = 5.0999999075429514E-5;
const Scalar iz_RH_hipassembly = 0.01103300042450428;
const Scalar m_RH_upperleg = 2.638000011444092;
const Scalar comx_RH_upperleg = 0.15073999762535095;
const Scalar comy_RH_upperleg = 0.026249999180436134;
const Scalar ix_RH_upperleg = 0.005495000164955854;
const Scalar ixy_RH_upperleg = 0.007418000139296055;
const Scalar ixz_RH_upperleg = 1.0199999815085903E-4;
const Scalar iy_RH_upperleg = 0.08713600039482117;
const Scalar iyz_RH_upperleg = -2.099999983329326E-5;
const Scalar iz_RH_upperleg = 0.08987099677324295;
const Scalar m_RH_lowerleg = 0.8809999823570251;
const Scalar comx_RH_lowerleg = 0.12540000677108765;
const Scalar comy_RH_lowerleg = -5.000000237487257E-4;
const Scalar comz_RH_lowerleg = 9.999999747378752E-5;
const Scalar ix_RH_lowerleg = 4.6800001291558146E-4;
const Scalar iy_RH_lowerleg = 0.026409000158309937;
const Scalar iz_RH_lowerleg = 0.026180999353528023;

}
}
#endif
