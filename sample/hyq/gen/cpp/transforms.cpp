#include "transforms.h"

using namespace HyQ::rcg;

// Constructors

MotionTransforms::MotionTransforms()
 :     fr_trunk_X_LF_foot(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_trunk_X_RF_foot(),
    fr_trunk_X_fr_RF_HAA(),
    fr_trunk_X_fr_RF_HFE(),
    fr_trunk_X_fr_RF_KFE(),
    fr_trunk_X_LH_foot(),
    fr_trunk_X_fr_LH_HAA(),
    fr_trunk_X_fr_LH_HFE(),
    fr_trunk_X_fr_LH_KFE(),
    fr_trunk_X_RH_foot(),
    fr_trunk_X_fr_RH_HAA(),
    fr_trunk_X_fr_RH_HFE(),
    fr_trunk_X_fr_RH_KFE(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LF_hipassembly(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg(),
    fr_RF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RF_hipassembly(),
    fr_RF_upperleg_X_fr_RF_hipassembly(),
    fr_RF_hipassembly_X_fr_RF_upperleg(),
    fr_RF_lowerleg_X_fr_RF_upperleg(),
    fr_RF_upperleg_X_fr_RF_lowerleg(),
    fr_LH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LH_hipassembly(),
    fr_LH_upperleg_X_fr_LH_hipassembly(),
    fr_LH_hipassembly_X_fr_LH_upperleg(),
    fr_LH_lowerleg_X_fr_LH_upperleg(),
    fr_LH_upperleg_X_fr_LH_lowerleg(),
    fr_RH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RH_hipassembly(),
    fr_RH_upperleg_X_fr_RH_hipassembly(),
    fr_RH_hipassembly_X_fr_RH_upperleg(),
    fr_RH_lowerleg_X_fr_RH_upperleg(),
    fr_RH_upperleg_X_fr_RH_lowerleg()
{}
void MotionTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

ForceTransforms::ForceTransforms()
 :     fr_trunk_X_LF_foot(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_trunk_X_RF_foot(),
    fr_trunk_X_fr_RF_HAA(),
    fr_trunk_X_fr_RF_HFE(),
    fr_trunk_X_fr_RF_KFE(),
    fr_trunk_X_LH_foot(),
    fr_trunk_X_fr_LH_HAA(),
    fr_trunk_X_fr_LH_HFE(),
    fr_trunk_X_fr_LH_KFE(),
    fr_trunk_X_RH_foot(),
    fr_trunk_X_fr_RH_HAA(),
    fr_trunk_X_fr_RH_HFE(),
    fr_trunk_X_fr_RH_KFE(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LF_hipassembly(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg(),
    fr_RF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RF_hipassembly(),
    fr_RF_upperleg_X_fr_RF_hipassembly(),
    fr_RF_hipassembly_X_fr_RF_upperleg(),
    fr_RF_lowerleg_X_fr_RF_upperleg(),
    fr_RF_upperleg_X_fr_RF_lowerleg(),
    fr_LH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LH_hipassembly(),
    fr_LH_upperleg_X_fr_LH_hipassembly(),
    fr_LH_hipassembly_X_fr_LH_upperleg(),
    fr_LH_lowerleg_X_fr_LH_upperleg(),
    fr_LH_upperleg_X_fr_LH_lowerleg(),
    fr_RH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RH_hipassembly(),
    fr_RH_upperleg_X_fr_RH_hipassembly(),
    fr_RH_hipassembly_X_fr_RH_upperleg(),
    fr_RH_lowerleg_X_fr_RH_upperleg(),
    fr_RH_upperleg_X_fr_RH_lowerleg()
{}
void ForceTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

HomogeneousTransforms::HomogeneousTransforms()
 :     fr_trunk_X_LF_foot(),
    fr_trunk_X_fr_LF_HAA(),
    fr_trunk_X_fr_LF_HFE(),
    fr_trunk_X_fr_LF_KFE(),
    fr_trunk_X_RF_foot(),
    fr_trunk_X_fr_RF_HAA(),
    fr_trunk_X_fr_RF_HFE(),
    fr_trunk_X_fr_RF_KFE(),
    fr_trunk_X_LH_foot(),
    fr_trunk_X_fr_LH_HAA(),
    fr_trunk_X_fr_LH_HFE(),
    fr_trunk_X_fr_LH_KFE(),
    fr_trunk_X_RH_foot(),
    fr_trunk_X_fr_RH_HAA(),
    fr_trunk_X_fr_RH_HFE(),
    fr_trunk_X_fr_RH_KFE(),
    fr_LF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LF_hipassembly(),
    fr_LF_upperleg_X_fr_LF_hipassembly(),
    fr_LF_hipassembly_X_fr_LF_upperleg(),
    fr_LF_lowerleg_X_fr_LF_upperleg(),
    fr_LF_upperleg_X_fr_LF_lowerleg(),
    fr_RF_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RF_hipassembly(),
    fr_RF_upperleg_X_fr_RF_hipassembly(),
    fr_RF_hipassembly_X_fr_RF_upperleg(),
    fr_RF_lowerleg_X_fr_RF_upperleg(),
    fr_RF_upperleg_X_fr_RF_lowerleg(),
    fr_LH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_LH_hipassembly(),
    fr_LH_upperleg_X_fr_LH_hipassembly(),
    fr_LH_hipassembly_X_fr_LH_upperleg(),
    fr_LH_lowerleg_X_fr_LH_upperleg(),
    fr_LH_upperleg_X_fr_LH_lowerleg(),
    fr_RH_hipassembly_X_fr_trunk(),
    fr_trunk_X_fr_RH_hipassembly(),
    fr_RH_upperleg_X_fr_RH_hipassembly(),
    fr_RH_hipassembly_X_fr_RH_upperleg(),
    fr_RH_lowerleg_X_fr_RH_upperleg(),
    fr_RH_upperleg_X_fr_RH_lowerleg()
{}
void HomogeneousTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

MotionTransforms::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_LF_foot& MotionTransforms::Type_fr_trunk_X_LF_foot::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(0,2) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(1,0) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(1,1) = cos_q_LF_HAA;
    (*this)(1,2) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(2,0) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(2,1) = -sin_q_LF_HAA;
    (*this)(2,2) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(3,0) = (- ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(3,1) = (- tx_LF_foot * sin_q_LF_HFE * sin_q_LF_KFE)+( tx_LF_foot * cos_q_LF_HFE * cos_q_LF_KFE)+( tx_LF_KFE * cos_q_LF_HFE)-( ty_LF_HAA * sin_q_LF_HAA)+ tx_LF_HFE;
    (*this)(3,2) = ( ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(3,3) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(3,5) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(4,0) = ((( tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE)) * sin_q_LF_KFE)+((( tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_KFE * cos_q_LF_HAA)) * cos_q_LF_KFE)-( tx_LF_foot * cos_q_LF_HAA);
    (*this)(4,1) = (- tx_LF_foot * sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * sin_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * sin_q_LF_HAA);
    (*this)(4,2) = ((( tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_KFE * cos_q_LF_HAA)) * sin_q_LF_KFE)+(((- tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE)) * cos_q_LF_KFE);
    (*this)(4,3) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(4,4) = cos_q_LF_HAA;
    (*this)(4,5) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(5,0) = (((( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * sin_q_LF_HFE)-( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE)) * sin_q_LF_KFE)+(((- tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * cos_q_LF_HFE)+( tx_LF_KFE * sin_q_LF_HAA)) * cos_q_LF_KFE)+( tx_LF_foot * sin_q_LF_HAA);
    (*this)(5,1) = (- tx_LF_foot * cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA);
    (*this)(5,2) = (((- tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * cos_q_LF_HFE)+( tx_LF_KFE * sin_q_LF_HAA)) * sin_q_LF_KFE)+((((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * sin_q_LF_HFE)+( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE)) * cos_q_LF_KFE);
    (*this)(5,3) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(5,4) = -sin_q_LF_HAA;
    (*this)(5,5) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = - ty_LF_HAA;    // Maxima DSL: -_k__ty_LF_HAA
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = -1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_LF_HAA;    // Maxima DSL: -_k__tx_LF_HAA
    (*this)(5,2) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LF_HAA& MotionTransforms::Type_fr_trunk_X_fr_LF_HAA::update(const state_t& q)
{
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LF_HFE& MotionTransforms::Type_fr_trunk_X_fr_LF_HFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(3,0) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(3,2) =  tx_LF_HFE-( ty_LF_HAA * sin_q_LF_HAA);
    (*this)(4,0) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(4,1) =  tx_LF_HFE * cos_q_LF_HAA;
    (*this)(4,2) =  tx_LF_HAA * sin_q_LF_HAA;
    (*this)(4,3) = -sin_q_LF_HAA;
    (*this)(4,5) = cos_q_LF_HAA;
    (*this)(5,0) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(5,1) =  ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA);
    (*this)(5,2) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(5,3) = -cos_q_LF_HAA;
    (*this)(5,5) = -sin_q_LF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LF_KFE& MotionTransforms::Type_fr_trunk_X_fr_LF_KFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = -sin_q_LF_HFE;
    (*this)(0,1) = -cos_q_LF_HFE;
    (*this)(1,0) = -sin_q_LF_HAA * cos_q_LF_HFE;
    (*this)(1,1) = sin_q_LF_HAA * sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(3,0) = - ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(3,1) =  ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(3,2) = ( tx_LF_KFE * cos_q_LF_HFE)-( ty_LF_HAA * sin_q_LF_HAA)+ tx_LF_HFE;
    (*this)(3,3) = -sin_q_LF_HFE;
    (*this)(3,4) = -cos_q_LF_HFE;
    (*this)(4,0) = ( tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE);
    (*this)(4,1) = (- tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)+( tx_LF_KFE * cos_q_LF_HAA);
    (*this)(4,2) = ( tx_LF_HAA * sin_q_LF_HAA)-( tx_LF_KFE * sin_q_LF_HAA * sin_q_LF_HFE);
    (*this)(4,3) = -sin_q_LF_HAA * cos_q_LF_HFE;
    (*this)(4,4) = sin_q_LF_HAA * sin_q_LF_HFE;
    (*this)(4,5) = cos_q_LF_HAA;
    (*this)(5,0) = (( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * sin_q_LF_HFE)-( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE);
    (*this)(5,1) = ( tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+(( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * cos_q_LF_HFE)-( tx_LF_KFE * sin_q_LF_HAA);
    (*this)(5,2) = ( tx_LF_HAA * cos_q_LF_HAA)-( tx_LF_KFE * cos_q_LF_HAA * sin_q_LF_HFE);
    (*this)(5,3) = -cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(5,4) = cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(5,5) = -sin_q_LF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_RF_foot::Type_fr_trunk_X_RF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_RF_foot& MotionTransforms::Type_fr_trunk_X_RF_foot::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(0,2) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,0) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
    (*this)(2,0) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(2,1) = sin_q_RF_HAA;
    (*this)(2,2) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(3,0) = (- ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-( ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(3,1) = (- tx_RF_foot * sin_q_RF_HFE * sin_q_RF_KFE)+( tx_RF_foot * cos_q_RF_HFE * cos_q_RF_KFE)+( tx_RF_KFE * cos_q_RF_HFE)+( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(3,2) = ( ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-( ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(3,3) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(3,5) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(4,0) = ((( tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE)) * sin_q_RF_KFE)+((( tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_KFE * cos_q_RF_HAA)) * cos_q_RF_KFE)-( tx_RF_foot * cos_q_RF_HAA);
    (*this)(4,1) = ( tx_RF_foot * sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+( tx_RF_foot * sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE)+( tx_RF_KFE * sin_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA);
    (*this)(4,2) = ((( tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_KFE * cos_q_RF_HAA)) * sin_q_RF_KFE)+(((- tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE)) * cos_q_RF_KFE);
    (*this)(4,3) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(4,5) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
    (*this)(5,0) = ((((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * sin_q_RF_HFE)+( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE)) * sin_q_RF_KFE)+((( tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+(((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * cos_q_RF_HFE)-( tx_RF_KFE * sin_q_RF_HAA)) * cos_q_RF_KFE)-( tx_RF_foot * sin_q_RF_HAA);
    (*this)(5,1) = (- tx_RF_foot * cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-( tx_RF_foot * cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE)-( tx_RF_KFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA);
    (*this)(5,2) = ((( tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+(((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * cos_q_RF_HFE)-( tx_RF_KFE * sin_q_RF_HAA)) * sin_q_RF_KFE)+(((((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE)) * cos_q_RF_KFE);
    (*this)(5,3) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(5,4) = sin_q_RF_HAA;
    (*this)(5,5) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RF_HAA::Type_fr_trunk_X_fr_RF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(5,2) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RF_HAA& MotionTransforms::Type_fr_trunk_X_fr_RF_HAA::update(const state_t& q)
{
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RF_HFE::Type_fr_trunk_X_fr_RF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RF_HFE& MotionTransforms::Type_fr_trunk_X_fr_RF_HFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(3,0) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(3,2) = ( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(4,0) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(4,1) =  tx_RF_HFE * cos_q_RF_HAA;
    (*this)(4,2) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(4,3) = sin_q_RF_HAA;
    (*this)(4,5) = cos_q_RF_HAA;
    (*this)(5,0) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(5,1) = ( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA;
    (*this)(5,2) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(5,3) = -cos_q_RF_HAA;
    (*this)(5,5) = sin_q_RF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RF_KFE::Type_fr_trunk_X_fr_RF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RF_KFE& MotionTransforms::Type_fr_trunk_X_fr_RF_KFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = -sin_q_RF_HFE;
    (*this)(0,1) = -cos_q_RF_HFE;
    (*this)(1,0) = sin_q_RF_HAA * cos_q_RF_HFE;
    (*this)(1,1) = -sin_q_RF_HAA * sin_q_RF_HFE;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(2,1) = cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(3,0) = - ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(3,1) =  ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(3,2) = ( tx_RF_KFE * cos_q_RF_HFE)+( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(3,3) = -sin_q_RF_HFE;
    (*this)(3,4) = -cos_q_RF_HFE;
    (*this)(4,0) = ( tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE);
    (*this)(4,1) = (- tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)+( tx_RF_KFE * cos_q_RF_HAA);
    (*this)(4,2) = ( tx_RF_KFE * sin_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA);
    (*this)(4,3) = sin_q_RF_HAA * cos_q_RF_HFE;
    (*this)(4,4) = -sin_q_RF_HAA * sin_q_RF_HFE;
    (*this)(4,5) = cos_q_RF_HAA;
    (*this)(5,0) = ((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * sin_q_RF_HFE)+( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE);
    (*this)(5,1) = (- tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * cos_q_RF_HFE)+( tx_RF_KFE * sin_q_RF_HAA);
    (*this)(5,2) = ( tx_RF_HAA * cos_q_RF_HAA)-( tx_RF_KFE * cos_q_RF_HAA * sin_q_RF_HFE);
    (*this)(5,3) = -cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(5,4) = cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(5,5) = sin_q_RF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_LH_foot::Type_fr_trunk_X_LH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_LH_foot& MotionTransforms::Type_fr_trunk_X_LH_foot::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(0,2) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(1,0) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(1,1) = cos_q_LH_HAA;
    (*this)(1,2) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(2,0) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(2,1) = -sin_q_LH_HAA;
    (*this)(2,2) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(3,0) = (- ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(3,1) = (- tx_LH_foot * sin_q_LH_HFE * sin_q_LH_KFE)+( tx_LH_foot * cos_q_LH_HFE * cos_q_LH_KFE)+( tx_LH_KFE * cos_q_LH_HFE)-( ty_LH_HAA * sin_q_LH_HAA)+ tx_LH_HFE;
    (*this)(3,2) = ( ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(3,3) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(3,5) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(4,0) = ((( tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE)) * sin_q_LH_KFE)+((( tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_KFE * cos_q_LH_HAA)) * cos_q_LH_KFE)-( tx_LH_foot * cos_q_LH_HAA);
    (*this)(4,1) = (- tx_LH_foot * sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * sin_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * sin_q_LH_HAA);
    (*this)(4,2) = ((( tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_KFE * cos_q_LH_HAA)) * sin_q_LH_KFE)+(((- tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE)) * cos_q_LH_KFE);
    (*this)(4,3) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(4,4) = cos_q_LH_HAA;
    (*this)(4,5) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(5,0) = (((( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * sin_q_LH_HFE)-( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE)) * sin_q_LH_KFE)+(((- tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * cos_q_LH_HFE)+( tx_LH_KFE * sin_q_LH_HAA)) * cos_q_LH_KFE)+( tx_LH_foot * sin_q_LH_HAA);
    (*this)(5,1) = (- tx_LH_foot * cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA);
    (*this)(5,2) = (((- tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * cos_q_LH_HFE)+( tx_LH_KFE * sin_q_LH_HAA)) * sin_q_LH_KFE)+((((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * sin_q_LH_HFE)+( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE)) * cos_q_LH_KFE);
    (*this)(5,3) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(5,4) = -sin_q_LH_HAA;
    (*this)(5,5) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LH_HAA::Type_fr_trunk_X_fr_LH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = - ty_LH_HAA;    // Maxima DSL: -_k__ty_LH_HAA
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = -1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_LH_HAA;    // Maxima DSL: -_k__tx_LH_HAA
    (*this)(5,2) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LH_HAA& MotionTransforms::Type_fr_trunk_X_fr_LH_HAA::update(const state_t& q)
{
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LH_HFE::Type_fr_trunk_X_fr_LH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LH_HFE& MotionTransforms::Type_fr_trunk_X_fr_LH_HFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(3,0) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(3,2) =  tx_LH_HFE-( ty_LH_HAA * sin_q_LH_HAA);
    (*this)(4,0) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(4,1) =  tx_LH_HFE * cos_q_LH_HAA;
    (*this)(4,2) =  tx_LH_HAA * sin_q_LH_HAA;
    (*this)(4,3) = -sin_q_LH_HAA;
    (*this)(4,5) = cos_q_LH_HAA;
    (*this)(5,0) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(5,1) =  ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA);
    (*this)(5,2) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(5,3) = -cos_q_LH_HAA;
    (*this)(5,5) = -sin_q_LH_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LH_KFE::Type_fr_trunk_X_fr_LH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LH_KFE& MotionTransforms::Type_fr_trunk_X_fr_LH_KFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = -sin_q_LH_HFE;
    (*this)(0,1) = -cos_q_LH_HFE;
    (*this)(1,0) = -sin_q_LH_HAA * cos_q_LH_HFE;
    (*this)(1,1) = sin_q_LH_HAA * sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(3,0) = - ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(3,1) =  ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(3,2) = ( tx_LH_KFE * cos_q_LH_HFE)-( ty_LH_HAA * sin_q_LH_HAA)+ tx_LH_HFE;
    (*this)(3,3) = -sin_q_LH_HFE;
    (*this)(3,4) = -cos_q_LH_HFE;
    (*this)(4,0) = ( tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE);
    (*this)(4,1) = (- tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)+( tx_LH_KFE * cos_q_LH_HAA);
    (*this)(4,2) = ( tx_LH_HAA * sin_q_LH_HAA)-( tx_LH_KFE * sin_q_LH_HAA * sin_q_LH_HFE);
    (*this)(4,3) = -sin_q_LH_HAA * cos_q_LH_HFE;
    (*this)(4,4) = sin_q_LH_HAA * sin_q_LH_HFE;
    (*this)(4,5) = cos_q_LH_HAA;
    (*this)(5,0) = (( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * sin_q_LH_HFE)-( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE);
    (*this)(5,1) = ( tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+(( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * cos_q_LH_HFE)-( tx_LH_KFE * sin_q_LH_HAA);
    (*this)(5,2) = ( tx_LH_HAA * cos_q_LH_HAA)-( tx_LH_KFE * cos_q_LH_HAA * sin_q_LH_HFE);
    (*this)(5,3) = -cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(5,4) = cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(5,5) = -sin_q_LH_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_RH_foot::Type_fr_trunk_X_RH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_RH_foot& MotionTransforms::Type_fr_trunk_X_RH_foot::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(0,2) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,0) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
    (*this)(2,0) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(2,1) = sin_q_RH_HAA;
    (*this)(2,2) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(3,0) = (- ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-( ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(3,1) = (- tx_RH_foot * sin_q_RH_HFE * sin_q_RH_KFE)+( tx_RH_foot * cos_q_RH_HFE * cos_q_RH_KFE)+( tx_RH_KFE * cos_q_RH_HFE)+( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(3,2) = ( ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-( ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(3,3) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(3,5) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(4,0) = ((( tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE)) * sin_q_RH_KFE)+((( tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_KFE * cos_q_RH_HAA)) * cos_q_RH_KFE)-( tx_RH_foot * cos_q_RH_HAA);
    (*this)(4,1) = ( tx_RH_foot * sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+( tx_RH_foot * sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE)+( tx_RH_KFE * sin_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA);
    (*this)(4,2) = ((( tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_KFE * cos_q_RH_HAA)) * sin_q_RH_KFE)+(((- tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE)) * cos_q_RH_KFE);
    (*this)(4,3) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(4,5) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
    (*this)(5,0) = ((((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * sin_q_RH_HFE)+( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE)) * sin_q_RH_KFE)+((( tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+(((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * cos_q_RH_HFE)-( tx_RH_KFE * sin_q_RH_HAA)) * cos_q_RH_KFE)-( tx_RH_foot * sin_q_RH_HAA);
    (*this)(5,1) = (- tx_RH_foot * cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-( tx_RH_foot * cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE)-( tx_RH_KFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA);
    (*this)(5,2) = ((( tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+(((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * cos_q_RH_HFE)-( tx_RH_KFE * sin_q_RH_HAA)) * sin_q_RH_KFE)+(((((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE)) * cos_q_RH_KFE);
    (*this)(5,3) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(5,4) = sin_q_RH_HAA;
    (*this)(5,5) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RH_HAA::Type_fr_trunk_X_fr_RH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(5,2) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RH_HAA& MotionTransforms::Type_fr_trunk_X_fr_RH_HAA::update(const state_t& q)
{
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RH_HFE::Type_fr_trunk_X_fr_RH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RH_HFE& MotionTransforms::Type_fr_trunk_X_fr_RH_HFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(3,0) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(3,2) = ( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(4,0) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(4,1) =  tx_RH_HFE * cos_q_RH_HAA;
    (*this)(4,2) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(4,3) = sin_q_RH_HAA;
    (*this)(4,5) = cos_q_RH_HAA;
    (*this)(5,0) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(5,1) = ( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA;
    (*this)(5,2) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(5,3) = -cos_q_RH_HAA;
    (*this)(5,5) = sin_q_RH_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RH_KFE::Type_fr_trunk_X_fr_RH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RH_KFE& MotionTransforms::Type_fr_trunk_X_fr_RH_KFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = -sin_q_RH_HFE;
    (*this)(0,1) = -cos_q_RH_HFE;
    (*this)(1,0) = sin_q_RH_HAA * cos_q_RH_HFE;
    (*this)(1,1) = -sin_q_RH_HAA * sin_q_RH_HFE;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(2,1) = cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(3,0) = - ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(3,1) =  ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(3,2) = ( tx_RH_KFE * cos_q_RH_HFE)+( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(3,3) = -sin_q_RH_HFE;
    (*this)(3,4) = -cos_q_RH_HFE;
    (*this)(4,0) = ( tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE);
    (*this)(4,1) = (- tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)+( tx_RH_KFE * cos_q_RH_HAA);
    (*this)(4,2) = ( tx_RH_KFE * sin_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA);
    (*this)(4,3) = sin_q_RH_HAA * cos_q_RH_HFE;
    (*this)(4,4) = -sin_q_RH_HAA * sin_q_RH_HFE;
    (*this)(4,5) = cos_q_RH_HAA;
    (*this)(5,0) = ((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * sin_q_RH_HFE)+( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE);
    (*this)(5,1) = (- tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * cos_q_RH_HFE)+( tx_RH_KFE * sin_q_RH_HAA);
    (*this)(5,2) = ( tx_RH_HAA * cos_q_RH_HAA)-( tx_RH_KFE * cos_q_RH_HAA * sin_q_RH_HFE);
    (*this)(5,3) = -cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(5,4) = cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(5,5) = sin_q_RH_HAA;
    return *this;
}
MotionTransforms::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LF_hipassembly_X_fr_trunk& MotionTransforms::Type_fr_LF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(0,1) = -sin_q_LF_HAA;
    (*this)(0,2) = -cos_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(1,2) = sin_q_LF_HAA;
    (*this)(3,0) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(3,1) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(3,2) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(3,4) = -sin_q_LF_HAA;
    (*this)(3,5) = -cos_q_LF_HAA;
    (*this)(4,0) =  ty_LF_HAA * sin_q_LF_HAA;
    (*this)(4,1) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(4,2) = - tx_LF_HAA * cos_q_LF_HAA;
    (*this)(4,4) = -cos_q_LF_HAA;
    (*this)(4,5) = sin_q_LF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,2) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LF_hipassembly& MotionTransforms::Type_fr_trunk_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,1) = sin_q_LF_HAA;
    (*this)(3,0) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(3,1) =  ty_LF_HAA * sin_q_LF_HAA;
    (*this)(4,0) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(4,1) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(4,3) = -sin_q_LF_HAA;
    (*this)(4,4) = -cos_q_LF_HAA;
    (*this)(5,0) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(5,1) = - tx_LF_HAA * cos_q_LF_HAA;
    (*this)(5,3) = -cos_q_LF_HAA;
    (*this)(5,4) = sin_q_LF_HAA;
    return *this;
}
MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_LF_HFE;    // Maxima DSL: -_k__tx_LF_HFE
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly& MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,2) = sin_q_LF_HFE;
    (*this)(1,0) = -sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HFE;
    (*this)(3,1) = - tx_LF_HFE * sin_q_LF_HFE;
    (*this)(3,3) = cos_q_LF_HFE;
    (*this)(3,5) = sin_q_LF_HFE;
    (*this)(4,1) = - tx_LF_HFE * cos_q_LF_HFE;
    (*this)(4,3) = -sin_q_LF_HFE;
    (*this)(4,5) = cos_q_LF_HFE;
    return *this;
}
MotionTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_LF_HFE;    // Maxima DSL: -_k__tx_LF_HFE
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg& MotionTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,1) = -sin_q_LF_HFE;
    (*this)(2,0) = sin_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HFE;
    (*this)(3,3) = cos_q_LF_HFE;
    (*this)(3,4) = -sin_q_LF_HFE;
    (*this)(4,0) = - tx_LF_HFE * sin_q_LF_HFE;
    (*this)(4,1) = - tx_LF_HFE * cos_q_LF_HFE;
    (*this)(5,3) = sin_q_LF_HFE;
    (*this)(5,4) = cos_q_LF_HFE;
    return *this;
}
MotionTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_LF_KFE;    // Maxima DSL: -_k__tx_LF_KFE
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg& MotionTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = sin_q_LF_KFE;
    (*this)(1,0) = -sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    (*this)(3,2) =  tx_LF_KFE * sin_q_LF_KFE;
    (*this)(3,3) = cos_q_LF_KFE;
    (*this)(3,4) = sin_q_LF_KFE;
    (*this)(4,2) =  tx_LF_KFE * cos_q_LF_KFE;
    (*this)(4,3) = -sin_q_LF_KFE;
    (*this)(4,4) = cos_q_LF_KFE;
    return *this;
}
MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = - tx_LF_KFE;    // Maxima DSL: -_k__tx_LF_KFE
    (*this)(4,5) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg& MotionTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = -sin_q_LF_KFE;
    (*this)(1,0) = sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    (*this)(3,3) = cos_q_LF_KFE;
    (*this)(3,4) = -sin_q_LF_KFE;
    (*this)(4,3) = sin_q_LF_KFE;
    (*this)(4,4) = cos_q_LF_KFE;
    (*this)(5,0) =  tx_LF_KFE * sin_q_LF_KFE;
    (*this)(5,1) =  tx_LF_KFE * cos_q_LF_KFE;
    return *this;
}
MotionTransforms::Type_fr_RF_hipassembly_X_fr_trunk::Type_fr_RF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(5,3) = 1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RF_hipassembly_X_fr_trunk& MotionTransforms::Type_fr_RF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(0,1) = sin_q_RF_HAA;
    (*this)(0,2) = -cos_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = sin_q_RF_HAA;
    (*this)(3,0) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(3,1) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(3,2) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(3,4) = sin_q_RF_HAA;
    (*this)(3,5) = -cos_q_RF_HAA;
    (*this)(4,0) =  ty_RF_HAA * sin_q_RF_HAA;
    (*this)(4,1) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(4,2) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(4,5) = sin_q_RF_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RF_hipassembly::Type_fr_trunk_X_fr_RF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,2) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RF_hipassembly& MotionTransforms::Type_fr_trunk_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,1) = sin_q_RF_HAA;
    (*this)(3,0) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(3,1) =  ty_RF_HAA * sin_q_RF_HAA;
    (*this)(4,0) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(4,1) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(4,3) = sin_q_RF_HAA;
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(5,0) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(5,1) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(5,3) = -cos_q_RF_HAA;
    (*this)(5,4) = sin_q_RF_HAA;
    return *this;
}
MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::Type_fr_RF_upperleg_X_fr_RF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_RF_HFE;    // Maxima DSL: _k__tx_RF_HFE
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly& MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,2) = -sin_q_RF_HFE;
    (*this)(1,0) = -sin_q_RF_HFE;
    (*this)(1,2) = -cos_q_RF_HFE;
    (*this)(3,1) =  tx_RF_HFE * sin_q_RF_HFE;
    (*this)(3,3) = cos_q_RF_HFE;
    (*this)(3,5) = -sin_q_RF_HFE;
    (*this)(4,1) =  tx_RF_HFE * cos_q_RF_HFE;
    (*this)(4,3) = -sin_q_RF_HFE;
    (*this)(4,5) = -cos_q_RF_HFE;
    return *this;
}
MotionTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::Type_fr_RF_hipassembly_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_RF_HFE;    // Maxima DSL: _k__tx_RF_HFE
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg& MotionTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,1) = -sin_q_RF_HFE;
    (*this)(2,0) = -sin_q_RF_HFE;
    (*this)(2,1) = -cos_q_RF_HFE;
    (*this)(3,3) = cos_q_RF_HFE;
    (*this)(3,4) = -sin_q_RF_HFE;
    (*this)(4,0) =  tx_RF_HFE * sin_q_RF_HFE;
    (*this)(4,1) =  tx_RF_HFE * cos_q_RF_HFE;
    (*this)(5,3) = -sin_q_RF_HFE;
    (*this)(5,4) = -cos_q_RF_HFE;
    return *this;
}
MotionTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::Type_fr_RF_lowerleg_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_RF_KFE;    // Maxima DSL: -_k__tx_RF_KFE
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg& MotionTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = sin_q_RF_KFE;
    (*this)(1,0) = -sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    (*this)(3,2) =  tx_RF_KFE * sin_q_RF_KFE;
    (*this)(3,3) = cos_q_RF_KFE;
    (*this)(3,4) = sin_q_RF_KFE;
    (*this)(4,2) =  tx_RF_KFE * cos_q_RF_KFE;
    (*this)(4,3) = -sin_q_RF_KFE;
    (*this)(4,4) = cos_q_RF_KFE;
    return *this;
}
MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::Type_fr_RF_upperleg_X_fr_RF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = - tx_RF_KFE;    // Maxima DSL: -_k__tx_RF_KFE
    (*this)(4,5) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg& MotionTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = -sin_q_RF_KFE;
    (*this)(1,0) = sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    (*this)(3,3) = cos_q_RF_KFE;
    (*this)(3,4) = -sin_q_RF_KFE;
    (*this)(4,3) = sin_q_RF_KFE;
    (*this)(4,4) = cos_q_RF_KFE;
    (*this)(5,0) =  tx_RF_KFE * sin_q_RF_KFE;
    (*this)(5,1) =  tx_RF_KFE * cos_q_RF_KFE;
    return *this;
}
MotionTransforms::Type_fr_LH_hipassembly_X_fr_trunk::Type_fr_LH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LH_hipassembly_X_fr_trunk& MotionTransforms::Type_fr_LH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(0,1) = -sin_q_LH_HAA;
    (*this)(0,2) = -cos_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(1,2) = sin_q_LH_HAA;
    (*this)(3,0) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(3,1) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(3,2) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(3,4) = -sin_q_LH_HAA;
    (*this)(3,5) = -cos_q_LH_HAA;
    (*this)(4,0) =  ty_LH_HAA * sin_q_LH_HAA;
    (*this)(4,1) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(4,2) = - tx_LH_HAA * cos_q_LH_HAA;
    (*this)(4,4) = -cos_q_LH_HAA;
    (*this)(4,5) = sin_q_LH_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_LH_hipassembly::Type_fr_trunk_X_fr_LH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,2) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_LH_hipassembly& MotionTransforms::Type_fr_trunk_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,1) = sin_q_LH_HAA;
    (*this)(3,0) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(3,1) =  ty_LH_HAA * sin_q_LH_HAA;
    (*this)(4,0) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(4,1) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(4,3) = -sin_q_LH_HAA;
    (*this)(4,4) = -cos_q_LH_HAA;
    (*this)(5,0) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(5,1) = - tx_LH_HAA * cos_q_LH_HAA;
    (*this)(5,3) = -cos_q_LH_HAA;
    (*this)(5,4) = sin_q_LH_HAA;
    return *this;
}
MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::Type_fr_LH_upperleg_X_fr_LH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_LH_HFE;    // Maxima DSL: -_k__tx_LH_HFE
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly& MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,2) = sin_q_LH_HFE;
    (*this)(1,0) = -sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HFE;
    (*this)(3,1) = - tx_LH_HFE * sin_q_LH_HFE;
    (*this)(3,3) = cos_q_LH_HFE;
    (*this)(3,5) = sin_q_LH_HFE;
    (*this)(4,1) = - tx_LH_HFE * cos_q_LH_HFE;
    (*this)(4,3) = -sin_q_LH_HFE;
    (*this)(4,5) = cos_q_LH_HFE;
    return *this;
}
MotionTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::Type_fr_LH_hipassembly_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_LH_HFE;    // Maxima DSL: -_k__tx_LH_HFE
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg& MotionTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,1) = -sin_q_LH_HFE;
    (*this)(2,0) = sin_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HFE;
    (*this)(3,3) = cos_q_LH_HFE;
    (*this)(3,4) = -sin_q_LH_HFE;
    (*this)(4,0) = - tx_LH_HFE * sin_q_LH_HFE;
    (*this)(4,1) = - tx_LH_HFE * cos_q_LH_HFE;
    (*this)(5,3) = sin_q_LH_HFE;
    (*this)(5,4) = cos_q_LH_HFE;
    return *this;
}
MotionTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::Type_fr_LH_lowerleg_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_LH_KFE;    // Maxima DSL: -_k__tx_LH_KFE
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg& MotionTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = sin_q_LH_KFE;
    (*this)(1,0) = -sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    (*this)(3,2) =  tx_LH_KFE * sin_q_LH_KFE;
    (*this)(3,3) = cos_q_LH_KFE;
    (*this)(3,4) = sin_q_LH_KFE;
    (*this)(4,2) =  tx_LH_KFE * cos_q_LH_KFE;
    (*this)(4,3) = -sin_q_LH_KFE;
    (*this)(4,4) = cos_q_LH_KFE;
    return *this;
}
MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::Type_fr_LH_upperleg_X_fr_LH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = - tx_LH_KFE;    // Maxima DSL: -_k__tx_LH_KFE
    (*this)(4,5) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg& MotionTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = -sin_q_LH_KFE;
    (*this)(1,0) = sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    (*this)(3,3) = cos_q_LH_KFE;
    (*this)(3,4) = -sin_q_LH_KFE;
    (*this)(4,3) = sin_q_LH_KFE;
    (*this)(4,4) = cos_q_LH_KFE;
    (*this)(5,0) =  tx_LH_KFE * sin_q_LH_KFE;
    (*this)(5,1) =  tx_LH_KFE * cos_q_LH_KFE;
    return *this;
}
MotionTransforms::Type_fr_RH_hipassembly_X_fr_trunk::Type_fr_RH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(5,3) = 1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RH_hipassembly_X_fr_trunk& MotionTransforms::Type_fr_RH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(0,1) = sin_q_RH_HAA;
    (*this)(0,2) = -cos_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = sin_q_RH_HAA;
    (*this)(3,0) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(3,1) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(3,2) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(3,4) = sin_q_RH_HAA;
    (*this)(3,5) = -cos_q_RH_HAA;
    (*this)(4,0) =  ty_RH_HAA * sin_q_RH_HAA;
    (*this)(4,1) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(4,2) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(4,5) = sin_q_RH_HAA;
    return *this;
}
MotionTransforms::Type_fr_trunk_X_fr_RH_hipassembly::Type_fr_trunk_X_fr_RH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,2) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_trunk_X_fr_RH_hipassembly& MotionTransforms::Type_fr_trunk_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,1) = sin_q_RH_HAA;
    (*this)(3,0) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(3,1) =  ty_RH_HAA * sin_q_RH_HAA;
    (*this)(4,0) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(4,1) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(4,3) = sin_q_RH_HAA;
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(5,0) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(5,1) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(5,3) = -cos_q_RH_HAA;
    (*this)(5,4) = sin_q_RH_HAA;
    return *this;
}
MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::Type_fr_RH_upperleg_X_fr_RH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_RH_HFE;    // Maxima DSL: _k__tx_RH_HFE
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly& MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,2) = -sin_q_RH_HFE;
    (*this)(1,0) = -sin_q_RH_HFE;
    (*this)(1,2) = -cos_q_RH_HFE;
    (*this)(3,1) =  tx_RH_HFE * sin_q_RH_HFE;
    (*this)(3,3) = cos_q_RH_HFE;
    (*this)(3,5) = -sin_q_RH_HFE;
    (*this)(4,1) =  tx_RH_HFE * cos_q_RH_HFE;
    (*this)(4,3) = -sin_q_RH_HFE;
    (*this)(4,5) = -cos_q_RH_HFE;
    return *this;
}
MotionTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::Type_fr_RH_hipassembly_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_RH_HFE;    // Maxima DSL: _k__tx_RH_HFE
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg& MotionTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,1) = -sin_q_RH_HFE;
    (*this)(2,0) = -sin_q_RH_HFE;
    (*this)(2,1) = -cos_q_RH_HFE;
    (*this)(3,3) = cos_q_RH_HFE;
    (*this)(3,4) = -sin_q_RH_HFE;
    (*this)(4,0) =  tx_RH_HFE * sin_q_RH_HFE;
    (*this)(4,1) =  tx_RH_HFE * cos_q_RH_HFE;
    (*this)(5,3) = -sin_q_RH_HFE;
    (*this)(5,4) = -cos_q_RH_HFE;
    return *this;
}
MotionTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::Type_fr_RH_lowerleg_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = - tx_RH_KFE;    // Maxima DSL: -_k__tx_RH_KFE
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg& MotionTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = sin_q_RH_KFE;
    (*this)(1,0) = -sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    (*this)(3,2) =  tx_RH_KFE * sin_q_RH_KFE;
    (*this)(3,3) = cos_q_RH_KFE;
    (*this)(3,4) = sin_q_RH_KFE;
    (*this)(4,2) =  tx_RH_KFE * cos_q_RH_KFE;
    (*this)(4,3) = -sin_q_RH_KFE;
    (*this)(4,4) = cos_q_RH_KFE;
    return *this;
}
MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::Type_fr_RH_upperleg_X_fr_RH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = - tx_RH_KFE;    // Maxima DSL: -_k__tx_RH_KFE
    (*this)(4,5) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg& MotionTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = -sin_q_RH_KFE;
    (*this)(1,0) = sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    (*this)(3,3) = cos_q_RH_KFE;
    (*this)(3,4) = -sin_q_RH_KFE;
    (*this)(4,3) = sin_q_RH_KFE;
    (*this)(4,4) = cos_q_RH_KFE;
    (*this)(5,0) =  tx_RH_KFE * sin_q_RH_KFE;
    (*this)(5,1) =  tx_RH_KFE * cos_q_RH_KFE;
    return *this;
}

ForceTransforms::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_LF_foot& ForceTransforms::Type_fr_trunk_X_LF_foot::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(0,2) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(0,3) = (- ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(0,4) = (- tx_LF_foot * sin_q_LF_HFE * sin_q_LF_KFE)+( tx_LF_foot * cos_q_LF_HFE * cos_q_LF_KFE)+( tx_LF_KFE * cos_q_LF_HFE)-( ty_LF_HAA * sin_q_LF_HAA)+ tx_LF_HFE;
    (*this)(0,5) = ( ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(1,0) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(1,1) = cos_q_LF_HAA;
    (*this)(1,2) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(1,3) = ((( tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE)) * sin_q_LF_KFE)+((( tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_KFE * cos_q_LF_HAA)) * cos_q_LF_KFE)-( tx_LF_foot * cos_q_LF_HAA);
    (*this)(1,4) = (- tx_LF_foot * sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * sin_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * sin_q_LF_HAA);
    (*this)(1,5) = ((( tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_KFE * cos_q_LF_HAA)) * sin_q_LF_KFE)+(((- tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)-( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE)) * cos_q_LF_KFE);
    (*this)(2,0) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(2,1) = -sin_q_LF_HAA;
    (*this)(2,2) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(2,3) = (((( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * sin_q_LF_HFE)-( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE)) * sin_q_LF_KFE)+(((- tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * cos_q_LF_HFE)+( tx_LF_KFE * sin_q_LF_HAA)) * cos_q_LF_KFE)+( tx_LF_foot * sin_q_LF_HAA);
    (*this)(2,4) = (- tx_LF_foot * cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA);
    (*this)(2,5) = (((- tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * cos_q_LF_HFE)+( tx_LF_KFE * sin_q_LF_HAA)) * sin_q_LF_KFE)+((((( tx_LF_HFE * sin_q_LF_HAA)- ty_LF_HAA) * sin_q_LF_HFE)+( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE)) * cos_q_LF_KFE);
    (*this)(3,3) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(3,5) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(4,3) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(4,4) = cos_q_LF_HAA;
    (*this)(4,5) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(5,3) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(5,4) = -sin_q_LF_HAA;
    (*this)(5,5) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = - ty_LF_HAA;    // Maxima DSL: -_k__ty_LF_HAA
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_LF_HAA;    // Maxima DSL: -_k__tx_LF_HAA
    (*this)(2,5) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = -1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LF_HAA& ForceTransforms::Type_fr_trunk_X_fr_LF_HAA::update(const state_t& q)
{
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LF_HFE& ForceTransforms::Type_fr_trunk_X_fr_LF_HFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(0,3) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(0,5) =  tx_LF_HFE-( ty_LF_HAA * sin_q_LF_HAA);
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(1,3) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(1,4) =  tx_LF_HFE * cos_q_LF_HAA;
    (*this)(1,5) =  tx_LF_HAA * sin_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(2,3) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(2,4) =  ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA);
    (*this)(2,5) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(4,3) = -sin_q_LF_HAA;
    (*this)(4,5) = cos_q_LF_HAA;
    (*this)(5,3) = -cos_q_LF_HAA;
    (*this)(5,5) = -sin_q_LF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LF_KFE& ForceTransforms::Type_fr_trunk_X_fr_LF_KFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = -sin_q_LF_HFE;
    (*this)(0,1) = -cos_q_LF_HFE;
    (*this)(0,3) = - ty_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(0,4) =  ty_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(0,5) = ( tx_LF_KFE * cos_q_LF_HFE)-( ty_LF_HAA * sin_q_LF_HAA)+ tx_LF_HFE;
    (*this)(1,0) = -sin_q_LF_HAA * cos_q_LF_HFE;
    (*this)(1,1) = sin_q_LF_HAA * sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(1,3) = ( tx_LF_HFE * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HAA * cos_q_LF_HAA * cos_q_LF_HFE);
    (*this)(1,4) = (- tx_LF_HAA * cos_q_LF_HAA * sin_q_LF_HFE)+( tx_LF_HFE * cos_q_LF_HAA * cos_q_LF_HFE)+( tx_LF_KFE * cos_q_LF_HAA);
    (*this)(1,5) = ( tx_LF_HAA * sin_q_LF_HAA)-( tx_LF_KFE * sin_q_LF_HAA * sin_q_LF_HFE);
    (*this)(2,0) = -cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(2,3) = (( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * sin_q_LF_HFE)-( tx_LF_HAA * sin_q_LF_HAA * cos_q_LF_HFE);
    (*this)(2,4) = ( tx_LF_HAA * sin_q_LF_HAA * sin_q_LF_HFE)+(( ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA)) * cos_q_LF_HFE)-( tx_LF_KFE * sin_q_LF_HAA);
    (*this)(2,5) = ( tx_LF_HAA * cos_q_LF_HAA)-( tx_LF_KFE * cos_q_LF_HAA * sin_q_LF_HFE);
    (*this)(3,3) = -sin_q_LF_HFE;
    (*this)(3,4) = -cos_q_LF_HFE;
    (*this)(4,3) = -sin_q_LF_HAA * cos_q_LF_HFE;
    (*this)(4,4) = sin_q_LF_HAA * sin_q_LF_HFE;
    (*this)(4,5) = cos_q_LF_HAA;
    (*this)(5,3) = -cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(5,4) = cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(5,5) = -sin_q_LF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_RF_foot::Type_fr_trunk_X_RF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_RF_foot& ForceTransforms::Type_fr_trunk_X_RF_foot::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(0,2) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(0,3) = (- ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-( ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(0,4) = (- tx_RF_foot * sin_q_RF_HFE * sin_q_RF_KFE)+( tx_RF_foot * cos_q_RF_HFE * cos_q_RF_KFE)+( tx_RF_KFE * cos_q_RF_HFE)+( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(0,5) = ( ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-( ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(1,0) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,3) = ((( tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE)) * sin_q_RF_KFE)+((( tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_KFE * cos_q_RF_HAA)) * cos_q_RF_KFE)-( tx_RF_foot * cos_q_RF_HAA);
    (*this)(1,4) = ( tx_RF_foot * sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+( tx_RF_foot * sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE)+( tx_RF_KFE * sin_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA);
    (*this)(1,5) = ((( tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_KFE * cos_q_RF_HAA)) * sin_q_RF_KFE)+(((- tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE)) * cos_q_RF_KFE);
    (*this)(2,0) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(2,1) = sin_q_RF_HAA;
    (*this)(2,2) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(2,3) = ((((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * sin_q_RF_HFE)+( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE)) * sin_q_RF_KFE)+((( tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+(((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * cos_q_RF_HFE)-( tx_RF_KFE * sin_q_RF_HAA)) * cos_q_RF_KFE)-( tx_RF_foot * sin_q_RF_HAA);
    (*this)(2,4) = (- tx_RF_foot * cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-( tx_RF_foot * cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE)-( tx_RF_KFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA);
    (*this)(2,5) = ((( tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+(((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * cos_q_RF_HFE)-( tx_RF_KFE * sin_q_RF_HAA)) * sin_q_RF_KFE)+(((((- tx_RF_HFE * sin_q_RF_HAA)- ty_RF_HAA) * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE)) * cos_q_RF_KFE);
    (*this)(3,3) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(3,5) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(4,3) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(4,5) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
    (*this)(5,3) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(5,4) = sin_q_RF_HAA;
    (*this)(5,5) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RF_HAA::Type_fr_trunk_X_fr_RF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(2,5) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RF_HAA& ForceTransforms::Type_fr_trunk_X_fr_RF_HAA::update(const state_t& q)
{
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RF_HFE::Type_fr_trunk_X_fr_RF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RF_HFE& ForceTransforms::Type_fr_trunk_X_fr_RF_HFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(0,3) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(0,5) = ( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(1,3) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(1,4) =  tx_RF_HFE * cos_q_RF_HAA;
    (*this)(1,5) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(2,3) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(2,4) = ( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA;
    (*this)(2,5) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(4,3) = sin_q_RF_HAA;
    (*this)(4,5) = cos_q_RF_HAA;
    (*this)(5,3) = -cos_q_RF_HAA;
    (*this)(5,5) = sin_q_RF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RF_KFE::Type_fr_trunk_X_fr_RF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RF_KFE& ForceTransforms::Type_fr_trunk_X_fr_RF_KFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = -sin_q_RF_HFE;
    (*this)(0,1) = -cos_q_RF_HFE;
    (*this)(0,3) = - ty_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(0,4) =  ty_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(0,5) = ( tx_RF_KFE * cos_q_RF_HFE)+( ty_RF_HAA * sin_q_RF_HAA)+ tx_RF_HFE;
    (*this)(1,0) = sin_q_RF_HAA * cos_q_RF_HFE;
    (*this)(1,1) = -sin_q_RF_HAA * sin_q_RF_HFE;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(1,3) = ( tx_RF_HFE * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HAA * cos_q_RF_HAA * cos_q_RF_HFE);
    (*this)(1,4) = (- tx_RF_HAA * cos_q_RF_HAA * sin_q_RF_HFE)+( tx_RF_HFE * cos_q_RF_HAA * cos_q_RF_HFE)+( tx_RF_KFE * cos_q_RF_HAA);
    (*this)(1,5) = ( tx_RF_KFE * sin_q_RF_HAA * sin_q_RF_HFE)-( tx_RF_HAA * sin_q_RF_HAA);
    (*this)(2,0) = -cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(2,1) = cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(2,3) = ((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * sin_q_RF_HFE)+( tx_RF_HAA * sin_q_RF_HAA * cos_q_RF_HFE);
    (*this)(2,4) = (- tx_RF_HAA * sin_q_RF_HAA * sin_q_RF_HFE)+((( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA) * cos_q_RF_HFE)+( tx_RF_KFE * sin_q_RF_HAA);
    (*this)(2,5) = ( tx_RF_HAA * cos_q_RF_HAA)-( tx_RF_KFE * cos_q_RF_HAA * sin_q_RF_HFE);
    (*this)(3,3) = -sin_q_RF_HFE;
    (*this)(3,4) = -cos_q_RF_HFE;
    (*this)(4,3) = sin_q_RF_HAA * cos_q_RF_HFE;
    (*this)(4,4) = -sin_q_RF_HAA * sin_q_RF_HFE;
    (*this)(4,5) = cos_q_RF_HAA;
    (*this)(5,3) = -cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(5,4) = cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(5,5) = sin_q_RF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_LH_foot::Type_fr_trunk_X_LH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_LH_foot& ForceTransforms::Type_fr_trunk_X_LH_foot::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(0,2) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(0,3) = (- ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(0,4) = (- tx_LH_foot * sin_q_LH_HFE * sin_q_LH_KFE)+( tx_LH_foot * cos_q_LH_HFE * cos_q_LH_KFE)+( tx_LH_KFE * cos_q_LH_HFE)-( ty_LH_HAA * sin_q_LH_HAA)+ tx_LH_HFE;
    (*this)(0,5) = ( ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(1,0) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(1,1) = cos_q_LH_HAA;
    (*this)(1,2) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(1,3) = ((( tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE)) * sin_q_LH_KFE)+((( tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_KFE * cos_q_LH_HAA)) * cos_q_LH_KFE)-( tx_LH_foot * cos_q_LH_HAA);
    (*this)(1,4) = (- tx_LH_foot * sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * sin_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * sin_q_LH_HAA);
    (*this)(1,5) = ((( tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_KFE * cos_q_LH_HAA)) * sin_q_LH_KFE)+(((- tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)-( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE)) * cos_q_LH_KFE);
    (*this)(2,0) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(2,1) = -sin_q_LH_HAA;
    (*this)(2,2) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(2,3) = (((( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * sin_q_LH_HFE)-( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE)) * sin_q_LH_KFE)+(((- tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * cos_q_LH_HFE)+( tx_LH_KFE * sin_q_LH_HAA)) * cos_q_LH_KFE)+( tx_LH_foot * sin_q_LH_HAA);
    (*this)(2,4) = (- tx_LH_foot * cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA);
    (*this)(2,5) = (((- tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * cos_q_LH_HFE)+( tx_LH_KFE * sin_q_LH_HAA)) * sin_q_LH_KFE)+((((( tx_LH_HFE * sin_q_LH_HAA)- ty_LH_HAA) * sin_q_LH_HFE)+( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE)) * cos_q_LH_KFE);
    (*this)(3,3) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(3,5) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(4,3) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(4,4) = cos_q_LH_HAA;
    (*this)(4,5) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(5,3) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(5,4) = -sin_q_LH_HAA;
    (*this)(5,5) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LH_HAA::Type_fr_trunk_X_fr_LH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) = - ty_LH_HAA;    // Maxima DSL: -_k__ty_LH_HAA
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_LH_HAA;    // Maxima DSL: -_k__tx_LH_HAA
    (*this)(2,5) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = -1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LH_HAA& ForceTransforms::Type_fr_trunk_X_fr_LH_HAA::update(const state_t& q)
{
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LH_HFE::Type_fr_trunk_X_fr_LH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LH_HFE& ForceTransforms::Type_fr_trunk_X_fr_LH_HFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(0,3) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(0,5) =  tx_LH_HFE-( ty_LH_HAA * sin_q_LH_HAA);
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(1,3) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(1,4) =  tx_LH_HFE * cos_q_LH_HAA;
    (*this)(1,5) =  tx_LH_HAA * sin_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(2,3) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(2,4) =  ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA);
    (*this)(2,5) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(4,3) = -sin_q_LH_HAA;
    (*this)(4,5) = cos_q_LH_HAA;
    (*this)(5,3) = -cos_q_LH_HAA;
    (*this)(5,5) = -sin_q_LH_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LH_KFE::Type_fr_trunk_X_fr_LH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LH_KFE& ForceTransforms::Type_fr_trunk_X_fr_LH_KFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = -sin_q_LH_HFE;
    (*this)(0,1) = -cos_q_LH_HFE;
    (*this)(0,3) = - ty_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(0,4) =  ty_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(0,5) = ( tx_LH_KFE * cos_q_LH_HFE)-( ty_LH_HAA * sin_q_LH_HAA)+ tx_LH_HFE;
    (*this)(1,0) = -sin_q_LH_HAA * cos_q_LH_HFE;
    (*this)(1,1) = sin_q_LH_HAA * sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(1,3) = ( tx_LH_HFE * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HAA * cos_q_LH_HAA * cos_q_LH_HFE);
    (*this)(1,4) = (- tx_LH_HAA * cos_q_LH_HAA * sin_q_LH_HFE)+( tx_LH_HFE * cos_q_LH_HAA * cos_q_LH_HFE)+( tx_LH_KFE * cos_q_LH_HAA);
    (*this)(1,5) = ( tx_LH_HAA * sin_q_LH_HAA)-( tx_LH_KFE * sin_q_LH_HAA * sin_q_LH_HFE);
    (*this)(2,0) = -cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(2,3) = (( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * sin_q_LH_HFE)-( tx_LH_HAA * sin_q_LH_HAA * cos_q_LH_HFE);
    (*this)(2,4) = ( tx_LH_HAA * sin_q_LH_HAA * sin_q_LH_HFE)+(( ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA)) * cos_q_LH_HFE)-( tx_LH_KFE * sin_q_LH_HAA);
    (*this)(2,5) = ( tx_LH_HAA * cos_q_LH_HAA)-( tx_LH_KFE * cos_q_LH_HAA * sin_q_LH_HFE);
    (*this)(3,3) = -sin_q_LH_HFE;
    (*this)(3,4) = -cos_q_LH_HFE;
    (*this)(4,3) = -sin_q_LH_HAA * cos_q_LH_HFE;
    (*this)(4,4) = sin_q_LH_HAA * sin_q_LH_HFE;
    (*this)(4,5) = cos_q_LH_HAA;
    (*this)(5,3) = -cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(5,4) = cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(5,5) = -sin_q_LH_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_RH_foot::Type_fr_trunk_X_RH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_RH_foot& ForceTransforms::Type_fr_trunk_X_RH_foot::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(0,2) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(0,3) = (- ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-( ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(0,4) = (- tx_RH_foot * sin_q_RH_HFE * sin_q_RH_KFE)+( tx_RH_foot * cos_q_RH_HFE * cos_q_RH_KFE)+( tx_RH_KFE * cos_q_RH_HFE)+( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(0,5) = ( ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-( ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(1,0) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,3) = ((( tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE)) * sin_q_RH_KFE)+((( tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_KFE * cos_q_RH_HAA)) * cos_q_RH_KFE)-( tx_RH_foot * cos_q_RH_HAA);
    (*this)(1,4) = ( tx_RH_foot * sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+( tx_RH_foot * sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE)+( tx_RH_KFE * sin_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA);
    (*this)(1,5) = ((( tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_KFE * cos_q_RH_HAA)) * sin_q_RH_KFE)+(((- tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE)) * cos_q_RH_KFE);
    (*this)(2,0) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(2,1) = sin_q_RH_HAA;
    (*this)(2,2) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(2,3) = ((((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * sin_q_RH_HFE)+( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE)) * sin_q_RH_KFE)+((( tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+(((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * cos_q_RH_HFE)-( tx_RH_KFE * sin_q_RH_HAA)) * cos_q_RH_KFE)-( tx_RH_foot * sin_q_RH_HAA);
    (*this)(2,4) = (- tx_RH_foot * cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-( tx_RH_foot * cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE)-( tx_RH_KFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA);
    (*this)(2,5) = ((( tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+(((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * cos_q_RH_HFE)-( tx_RH_KFE * sin_q_RH_HAA)) * sin_q_RH_KFE)+(((((- tx_RH_HFE * sin_q_RH_HAA)- ty_RH_HAA) * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE)) * cos_q_RH_KFE);
    (*this)(3,3) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(3,5) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(4,3) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(4,5) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
    (*this)(5,3) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(5,4) = sin_q_RH_HAA;
    (*this)(5,5) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RH_HAA::Type_fr_trunk_X_fr_RH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(2,5) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RH_HAA& ForceTransforms::Type_fr_trunk_X_fr_RH_HAA::update(const state_t& q)
{
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RH_HFE::Type_fr_trunk_X_fr_RH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = -1.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RH_HFE& ForceTransforms::Type_fr_trunk_X_fr_RH_HFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(0,3) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(0,5) = ( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(1,3) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(1,4) =  tx_RH_HFE * cos_q_RH_HAA;
    (*this)(1,5) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(2,3) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(2,4) = ( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA;
    (*this)(2,5) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(4,3) = sin_q_RH_HAA;
    (*this)(4,5) = cos_q_RH_HAA;
    (*this)(5,3) = -cos_q_RH_HAA;
    (*this)(5,5) = sin_q_RH_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RH_KFE::Type_fr_trunk_X_fr_RH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RH_KFE& ForceTransforms::Type_fr_trunk_X_fr_RH_KFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = -sin_q_RH_HFE;
    (*this)(0,1) = -cos_q_RH_HFE;
    (*this)(0,3) = - ty_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(0,4) =  ty_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(0,5) = ( tx_RH_KFE * cos_q_RH_HFE)+( ty_RH_HAA * sin_q_RH_HAA)+ tx_RH_HFE;
    (*this)(1,0) = sin_q_RH_HAA * cos_q_RH_HFE;
    (*this)(1,1) = -sin_q_RH_HAA * sin_q_RH_HFE;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(1,3) = ( tx_RH_HFE * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HAA * cos_q_RH_HAA * cos_q_RH_HFE);
    (*this)(1,4) = (- tx_RH_HAA * cos_q_RH_HAA * sin_q_RH_HFE)+( tx_RH_HFE * cos_q_RH_HAA * cos_q_RH_HFE)+( tx_RH_KFE * cos_q_RH_HAA);
    (*this)(1,5) = ( tx_RH_KFE * sin_q_RH_HAA * sin_q_RH_HFE)-( tx_RH_HAA * sin_q_RH_HAA);
    (*this)(2,0) = -cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(2,1) = cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(2,3) = ((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * sin_q_RH_HFE)+( tx_RH_HAA * sin_q_RH_HAA * cos_q_RH_HFE);
    (*this)(2,4) = (- tx_RH_HAA * sin_q_RH_HAA * sin_q_RH_HFE)+((( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA) * cos_q_RH_HFE)+( tx_RH_KFE * sin_q_RH_HAA);
    (*this)(2,5) = ( tx_RH_HAA * cos_q_RH_HAA)-( tx_RH_KFE * cos_q_RH_HAA * sin_q_RH_HFE);
    (*this)(3,3) = -sin_q_RH_HFE;
    (*this)(3,4) = -cos_q_RH_HFE;
    (*this)(4,3) = sin_q_RH_HAA * cos_q_RH_HFE;
    (*this)(4,4) = -sin_q_RH_HAA * sin_q_RH_HFE;
    (*this)(4,5) = cos_q_RH_HAA;
    (*this)(5,3) = -cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(5,4) = cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(5,5) = sin_q_RH_HAA;
    return *this;
}
ForceTransforms::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LF_hipassembly_X_fr_trunk& ForceTransforms::Type_fr_LF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(0,1) = -sin_q_LF_HAA;
    (*this)(0,2) = -cos_q_LF_HAA;
    (*this)(0,3) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(0,4) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(0,5) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(1,2) = sin_q_LF_HAA;
    (*this)(1,3) =  ty_LF_HAA * sin_q_LF_HAA;
    (*this)(1,4) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(1,5) = - tx_LF_HAA * cos_q_LF_HAA;
    (*this)(3,4) = -sin_q_LF_HAA;
    (*this)(3,5) = -cos_q_LF_HAA;
    (*this)(4,4) = -cos_q_LF_HAA;
    (*this)(4,5) = sin_q_LF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,5) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LF_hipassembly& ForceTransforms::Type_fr_trunk_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(0,3) = - ty_LF_HAA * cos_q_LF_HAA;
    (*this)(0,4) =  ty_LF_HAA * sin_q_LF_HAA;
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(1,3) =  tx_LF_HAA * cos_q_LF_HAA;
    (*this)(1,4) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,1) = sin_q_LF_HAA;
    (*this)(2,3) = - tx_LF_HAA * sin_q_LF_HAA;
    (*this)(2,4) = - tx_LF_HAA * cos_q_LF_HAA;
    (*this)(4,3) = -sin_q_LF_HAA;
    (*this)(4,4) = -cos_q_LF_HAA;
    (*this)(5,3) = -cos_q_LF_HAA;
    (*this)(5,4) = sin_q_LF_HAA;
    return *this;
}
ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_LF_HFE;    // Maxima DSL: -_k__tx_LF_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly& ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,2) = sin_q_LF_HFE;
    (*this)(0,4) = - tx_LF_HFE * sin_q_LF_HFE;
    (*this)(1,0) = -sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HFE;
    (*this)(1,4) = - tx_LF_HFE * cos_q_LF_HFE;
    (*this)(3,3) = cos_q_LF_HFE;
    (*this)(3,5) = sin_q_LF_HFE;
    (*this)(4,3) = -sin_q_LF_HFE;
    (*this)(4,5) = cos_q_LF_HFE;
    return *this;
}
ForceTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_LF_HFE;    // Maxima DSL: -_k__tx_LF_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg& ForceTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,1) = -sin_q_LF_HFE;
    (*this)(1,3) = - tx_LF_HFE * sin_q_LF_HFE;
    (*this)(1,4) = - tx_LF_HFE * cos_q_LF_HFE;
    (*this)(2,0) = sin_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HFE;
    (*this)(3,3) = cos_q_LF_HFE;
    (*this)(3,4) = -sin_q_LF_HFE;
    (*this)(5,3) = sin_q_LF_HFE;
    (*this)(5,4) = cos_q_LF_HFE;
    return *this;
}
ForceTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_LF_KFE;    // Maxima DSL: -_k__tx_LF_KFE
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg& ForceTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = sin_q_LF_KFE;
    (*this)(0,5) =  tx_LF_KFE * sin_q_LF_KFE;
    (*this)(1,0) = -sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    (*this)(1,5) =  tx_LF_KFE * cos_q_LF_KFE;
    (*this)(3,3) = cos_q_LF_KFE;
    (*this)(3,4) = sin_q_LF_KFE;
    (*this)(4,3) = -sin_q_LF_KFE;
    (*this)(4,4) = cos_q_LF_KFE;
    return *this;
}
ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = - tx_LF_KFE;    // Maxima DSL: -_k__tx_LF_KFE
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg& ForceTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = -sin_q_LF_KFE;
    (*this)(1,0) = sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    (*this)(2,3) =  tx_LF_KFE * sin_q_LF_KFE;
    (*this)(2,4) =  tx_LF_KFE * cos_q_LF_KFE;
    (*this)(3,3) = cos_q_LF_KFE;
    (*this)(3,4) = -sin_q_LF_KFE;
    (*this)(4,3) = sin_q_LF_KFE;
    (*this)(4,4) = cos_q_LF_KFE;
    return *this;
}
ForceTransforms::Type_fr_RF_hipassembly_X_fr_trunk::Type_fr_RF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RF_hipassembly_X_fr_trunk& ForceTransforms::Type_fr_RF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(0,1) = sin_q_RF_HAA;
    (*this)(0,2) = -cos_q_RF_HAA;
    (*this)(0,3) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(0,4) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(0,5) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = sin_q_RF_HAA;
    (*this)(1,3) =  ty_RF_HAA * sin_q_RF_HAA;
    (*this)(1,4) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(1,5) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(3,4) = sin_q_RF_HAA;
    (*this)(3,5) = -cos_q_RF_HAA;
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(4,5) = sin_q_RF_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RF_hipassembly::Type_fr_trunk_X_fr_RF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,5) = - ty_RF_HAA;    // Maxima DSL: -_k__ty_RF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RF_hipassembly& ForceTransforms::Type_fr_trunk_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(0,3) = - ty_RF_HAA * cos_q_RF_HAA;
    (*this)(0,4) =  ty_RF_HAA * sin_q_RF_HAA;
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,3) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(1,4) = - tx_RF_HAA * sin_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,1) = sin_q_RF_HAA;
    (*this)(2,3) =  tx_RF_HAA * sin_q_RF_HAA;
    (*this)(2,4) =  tx_RF_HAA * cos_q_RF_HAA;
    (*this)(4,3) = sin_q_RF_HAA;
    (*this)(4,4) = cos_q_RF_HAA;
    (*this)(5,3) = -cos_q_RF_HAA;
    (*this)(5,4) = sin_q_RF_HAA;
    return *this;
}
ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::Type_fr_RF_upperleg_X_fr_RF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_RF_HFE;    // Maxima DSL: _k__tx_RF_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly& ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,2) = -sin_q_RF_HFE;
    (*this)(0,4) =  tx_RF_HFE * sin_q_RF_HFE;
    (*this)(1,0) = -sin_q_RF_HFE;
    (*this)(1,2) = -cos_q_RF_HFE;
    (*this)(1,4) =  tx_RF_HFE * cos_q_RF_HFE;
    (*this)(3,3) = cos_q_RF_HFE;
    (*this)(3,5) = -sin_q_RF_HFE;
    (*this)(4,3) = -sin_q_RF_HFE;
    (*this)(4,5) = -cos_q_RF_HFE;
    return *this;
}
ForceTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::Type_fr_RF_hipassembly_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_RF_HFE;    // Maxima DSL: _k__tx_RF_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg& ForceTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,1) = -sin_q_RF_HFE;
    (*this)(1,3) =  tx_RF_HFE * sin_q_RF_HFE;
    (*this)(1,4) =  tx_RF_HFE * cos_q_RF_HFE;
    (*this)(2,0) = -sin_q_RF_HFE;
    (*this)(2,1) = -cos_q_RF_HFE;
    (*this)(3,3) = cos_q_RF_HFE;
    (*this)(3,4) = -sin_q_RF_HFE;
    (*this)(5,3) = -sin_q_RF_HFE;
    (*this)(5,4) = -cos_q_RF_HFE;
    return *this;
}
ForceTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::Type_fr_RF_lowerleg_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_RF_KFE;    // Maxima DSL: -_k__tx_RF_KFE
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg& ForceTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = sin_q_RF_KFE;
    (*this)(0,5) =  tx_RF_KFE * sin_q_RF_KFE;
    (*this)(1,0) = -sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    (*this)(1,5) =  tx_RF_KFE * cos_q_RF_KFE;
    (*this)(3,3) = cos_q_RF_KFE;
    (*this)(3,4) = sin_q_RF_KFE;
    (*this)(4,3) = -sin_q_RF_KFE;
    (*this)(4,4) = cos_q_RF_KFE;
    return *this;
}
ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::Type_fr_RF_upperleg_X_fr_RF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = - tx_RF_KFE;    // Maxima DSL: -_k__tx_RF_KFE
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg& ForceTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = -sin_q_RF_KFE;
    (*this)(1,0) = sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    (*this)(2,3) =  tx_RF_KFE * sin_q_RF_KFE;
    (*this)(2,4) =  tx_RF_KFE * cos_q_RF_KFE;
    (*this)(3,3) = cos_q_RF_KFE;
    (*this)(3,4) = -sin_q_RF_KFE;
    (*this)(4,3) = sin_q_RF_KFE;
    (*this)(4,4) = cos_q_RF_KFE;
    return *this;
}
ForceTransforms::Type_fr_LH_hipassembly_X_fr_trunk::Type_fr_LH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = -1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LH_hipassembly_X_fr_trunk& ForceTransforms::Type_fr_LH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(0,1) = -sin_q_LH_HAA;
    (*this)(0,2) = -cos_q_LH_HAA;
    (*this)(0,3) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(0,4) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(0,5) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(1,2) = sin_q_LH_HAA;
    (*this)(1,3) =  ty_LH_HAA * sin_q_LH_HAA;
    (*this)(1,4) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(1,5) = - tx_LH_HAA * cos_q_LH_HAA;
    (*this)(3,4) = -sin_q_LH_HAA;
    (*this)(3,5) = -cos_q_LH_HAA;
    (*this)(4,4) = -cos_q_LH_HAA;
    (*this)(4,5) = sin_q_LH_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_LH_hipassembly::Type_fr_trunk_X_fr_LH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,5) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = -1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_LH_hipassembly& ForceTransforms::Type_fr_trunk_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(0,3) = - ty_LH_HAA * cos_q_LH_HAA;
    (*this)(0,4) =  ty_LH_HAA * sin_q_LH_HAA;
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(1,3) =  tx_LH_HAA * cos_q_LH_HAA;
    (*this)(1,4) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,1) = sin_q_LH_HAA;
    (*this)(2,3) = - tx_LH_HAA * sin_q_LH_HAA;
    (*this)(2,4) = - tx_LH_HAA * cos_q_LH_HAA;
    (*this)(4,3) = -sin_q_LH_HAA;
    (*this)(4,4) = -cos_q_LH_HAA;
    (*this)(5,3) = -cos_q_LH_HAA;
    (*this)(5,4) = sin_q_LH_HAA;
    return *this;
}
ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::Type_fr_LH_upperleg_X_fr_LH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_LH_HFE;    // Maxima DSL: -_k__tx_LH_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly& ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,2) = sin_q_LH_HFE;
    (*this)(0,4) = - tx_LH_HFE * sin_q_LH_HFE;
    (*this)(1,0) = -sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HFE;
    (*this)(1,4) = - tx_LH_HFE * cos_q_LH_HFE;
    (*this)(3,3) = cos_q_LH_HFE;
    (*this)(3,5) = sin_q_LH_HFE;
    (*this)(4,3) = -sin_q_LH_HFE;
    (*this)(4,5) = cos_q_LH_HFE;
    return *this;
}
ForceTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::Type_fr_LH_hipassembly_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_LH_HFE;    // Maxima DSL: -_k__tx_LH_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg& ForceTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,1) = -sin_q_LH_HFE;
    (*this)(1,3) = - tx_LH_HFE * sin_q_LH_HFE;
    (*this)(1,4) = - tx_LH_HFE * cos_q_LH_HFE;
    (*this)(2,0) = sin_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HFE;
    (*this)(3,3) = cos_q_LH_HFE;
    (*this)(3,4) = -sin_q_LH_HFE;
    (*this)(5,3) = sin_q_LH_HFE;
    (*this)(5,4) = cos_q_LH_HFE;
    return *this;
}
ForceTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::Type_fr_LH_lowerleg_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_LH_KFE;    // Maxima DSL: -_k__tx_LH_KFE
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg& ForceTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = sin_q_LH_KFE;
    (*this)(0,5) =  tx_LH_KFE * sin_q_LH_KFE;
    (*this)(1,0) = -sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    (*this)(1,5) =  tx_LH_KFE * cos_q_LH_KFE;
    (*this)(3,3) = cos_q_LH_KFE;
    (*this)(3,4) = sin_q_LH_KFE;
    (*this)(4,3) = -sin_q_LH_KFE;
    (*this)(4,4) = cos_q_LH_KFE;
    return *this;
}
ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::Type_fr_LH_upperleg_X_fr_LH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = - tx_LH_KFE;    // Maxima DSL: -_k__tx_LH_KFE
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg& ForceTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = -sin_q_LH_KFE;
    (*this)(1,0) = sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    (*this)(2,3) =  tx_LH_KFE * sin_q_LH_KFE;
    (*this)(2,4) =  tx_LH_KFE * cos_q_LH_KFE;
    (*this)(3,3) = cos_q_LH_KFE;
    (*this)(3,4) = -sin_q_LH_KFE;
    (*this)(4,3) = sin_q_LH_KFE;
    (*this)(4,4) = cos_q_LH_KFE;
    return *this;
}
ForceTransforms::Type_fr_RH_hipassembly_X_fr_trunk::Type_fr_RH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 1.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RH_hipassembly_X_fr_trunk& ForceTransforms::Type_fr_RH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(0,1) = sin_q_RH_HAA;
    (*this)(0,2) = -cos_q_RH_HAA;
    (*this)(0,3) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(0,4) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(0,5) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = sin_q_RH_HAA;
    (*this)(1,3) =  ty_RH_HAA * sin_q_RH_HAA;
    (*this)(1,4) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(1,5) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(3,4) = sin_q_RH_HAA;
    (*this)(3,5) = -cos_q_RH_HAA;
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(4,5) = sin_q_RH_HAA;
    return *this;
}
ForceTransforms::Type_fr_trunk_X_fr_RH_hipassembly::Type_fr_trunk_X_fr_RH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,5) = - ty_RH_HAA;    // Maxima DSL: -_k__ty_RH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 1.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_trunk_X_fr_RH_hipassembly& ForceTransforms::Type_fr_trunk_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(0,3) = - ty_RH_HAA * cos_q_RH_HAA;
    (*this)(0,4) =  ty_RH_HAA * sin_q_RH_HAA;
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,3) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(1,4) = - tx_RH_HAA * sin_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,1) = sin_q_RH_HAA;
    (*this)(2,3) =  tx_RH_HAA * sin_q_RH_HAA;
    (*this)(2,4) =  tx_RH_HAA * cos_q_RH_HAA;
    (*this)(4,3) = sin_q_RH_HAA;
    (*this)(4,4) = cos_q_RH_HAA;
    (*this)(5,3) = -cos_q_RH_HAA;
    (*this)(5,4) = sin_q_RH_HAA;
    return *this;
}
ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::Type_fr_RH_upperleg_X_fr_RH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_RH_HFE;    // Maxima DSL: _k__tx_RH_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly& ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,2) = -sin_q_RH_HFE;
    (*this)(0,4) =  tx_RH_HFE * sin_q_RH_HFE;
    (*this)(1,0) = -sin_q_RH_HFE;
    (*this)(1,2) = -cos_q_RH_HFE;
    (*this)(1,4) =  tx_RH_HFE * cos_q_RH_HFE;
    (*this)(3,3) = cos_q_RH_HFE;
    (*this)(3,5) = -sin_q_RH_HFE;
    (*this)(4,3) = -sin_q_RH_HFE;
    (*this)(4,5) = -cos_q_RH_HFE;
    return *this;
}
ForceTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::Type_fr_RH_hipassembly_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,5) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_RH_HFE;    // Maxima DSL: _k__tx_RH_HFE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg& ForceTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,1) = -sin_q_RH_HFE;
    (*this)(1,3) =  tx_RH_HFE * sin_q_RH_HFE;
    (*this)(1,4) =  tx_RH_HFE * cos_q_RH_HFE;
    (*this)(2,0) = -sin_q_RH_HFE;
    (*this)(2,1) = -cos_q_RH_HFE;
    (*this)(3,3) = cos_q_RH_HFE;
    (*this)(3,4) = -sin_q_RH_HFE;
    (*this)(5,3) = -sin_q_RH_HFE;
    (*this)(5,4) = -cos_q_RH_HFE;
    return *this;
}
ForceTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::Type_fr_RH_lowerleg_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = - tx_RH_KFE;    // Maxima DSL: -_k__tx_RH_KFE
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg& ForceTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = sin_q_RH_KFE;
    (*this)(0,5) =  tx_RH_KFE * sin_q_RH_KFE;
    (*this)(1,0) = -sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    (*this)(1,5) =  tx_RH_KFE * cos_q_RH_KFE;
    (*this)(3,3) = cos_q_RH_KFE;
    (*this)(3,4) = sin_q_RH_KFE;
    (*this)(4,3) = -sin_q_RH_KFE;
    (*this)(4,4) = cos_q_RH_KFE;
    return *this;
}
ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::Type_fr_RH_upperleg_X_fr_RH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = - tx_RH_KFE;    // Maxima DSL: -_k__tx_RH_KFE
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg& ForceTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = -sin_q_RH_KFE;
    (*this)(1,0) = sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    (*this)(2,3) =  tx_RH_KFE * sin_q_RH_KFE;
    (*this)(2,4) =  tx_RH_KFE * cos_q_RH_KFE;
    (*this)(3,3) = cos_q_RH_KFE;
    (*this)(3,4) = -sin_q_RH_KFE;
    (*this)(4,3) = sin_q_RH_KFE;
    (*this)(4,4) = cos_q_RH_KFE;
    return *this;
}

HomogeneousTransforms::Type_fr_trunk_X_LF_foot::Type_fr_trunk_X_LF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_LF_foot& HomogeneousTransforms::Type_fr_trunk_X_LF_foot::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(0,2) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(0,3) = (- tx_LF_foot * cos_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * sin_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * sin_q_LF_HFE)+ tx_LF_HAA;
    (*this)(1,0) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(1,1) = cos_q_LF_HAA;
    (*this)(1,2) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(1,3) = ( tx_LF_foot * sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * sin_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_HFE * sin_q_LF_HAA)+ ty_LF_HAA;
    (*this)(2,0) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
    (*this)(2,1) = -sin_q_LF_HAA;
    (*this)(2,2) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
    (*this)(2,3) = ( tx_LF_foot * cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE)-( tx_LF_foot * cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( tx_LF_KFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HAA::Type_fr_trunk_X_fr_LF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HAA& HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HAA::update(const state_t& q)
{
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HFE::Type_fr_trunk_X_fr_LF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HFE& HomogeneousTransforms::Type_fr_trunk_X_fr_LF_HFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(1,3) =  ty_LF_HAA-( tx_LF_HFE * sin_q_LF_HAA);
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(2,3) = - tx_LF_HFE * cos_q_LF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LF_KFE::Type_fr_trunk_X_fr_LF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LF_KFE& HomogeneousTransforms::Type_fr_trunk_X_fr_LF_KFE::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = -sin_q_LF_HFE;
    (*this)(0,1) = -cos_q_LF_HFE;
    (*this)(0,3) =  tx_LF_HAA-( tx_LF_KFE * sin_q_LF_HFE);
    (*this)(1,0) = -sin_q_LF_HAA * cos_q_LF_HFE;
    (*this)(1,1) = sin_q_LF_HAA * sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HAA;
    (*this)(1,3) = (- tx_LF_KFE * sin_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_HFE * sin_q_LF_HAA)+ ty_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA * cos_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HAA * sin_q_LF_HFE;
    (*this)(2,2) = -sin_q_LF_HAA;
    (*this)(2,3) = (- tx_LF_KFE * cos_q_LF_HAA * cos_q_LF_HFE)-( tx_LF_HFE * cos_q_LF_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_RF_foot::Type_fr_trunk_X_RF_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_RF_foot& HomogeneousTransforms::Type_fr_trunk_X_RF_foot::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(0,2) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(0,3) = (- tx_RF_foot * cos_q_RF_HFE * sin_q_RF_KFE)-( tx_RF_foot * sin_q_RF_HFE * cos_q_RF_KFE)-( tx_RF_KFE * sin_q_RF_HFE)+ tx_RF_HAA;
    (*this)(1,0) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
    (*this)(1,3) = (- tx_RF_foot * sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)+( tx_RF_foot * sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)+( tx_RF_KFE * sin_q_RF_HAA * cos_q_RF_HFE)+( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA;
    (*this)(2,0) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
    (*this)(2,1) = sin_q_RF_HAA;
    (*this)(2,2) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
    (*this)(2,3) = ( tx_RF_foot * cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-( tx_RF_foot * cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-( tx_RF_KFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HAA::Type_fr_trunk_X_fr_RF_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_RF_HAA;    // Maxima DSL: _k__ty_RF_HAA
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HAA& HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HAA::update(const state_t& q)
{
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HFE::Type_fr_trunk_X_fr_RF_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HFE& HomogeneousTransforms::Type_fr_trunk_X_fr_RF_HFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(1,3) = ( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(2,3) = - tx_RF_HFE * cos_q_RF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RF_KFE::Type_fr_trunk_X_fr_RF_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RF_KFE& HomogeneousTransforms::Type_fr_trunk_X_fr_RF_KFE::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = -sin_q_RF_HFE;
    (*this)(0,1) = -cos_q_RF_HFE;
    (*this)(0,3) =  tx_RF_HAA-( tx_RF_KFE * sin_q_RF_HFE);
    (*this)(1,0) = sin_q_RF_HAA * cos_q_RF_HFE;
    (*this)(1,1) = -sin_q_RF_HAA * sin_q_RF_HFE;
    (*this)(1,2) = cos_q_RF_HAA;
    (*this)(1,3) = ( tx_RF_KFE * sin_q_RF_HAA * cos_q_RF_HFE)+( tx_RF_HFE * sin_q_RF_HAA)+ ty_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA * cos_q_RF_HFE;
    (*this)(2,1) = cos_q_RF_HAA * sin_q_RF_HFE;
    (*this)(2,2) = sin_q_RF_HAA;
    (*this)(2,3) = (- tx_RF_KFE * cos_q_RF_HAA * cos_q_RF_HFE)-( tx_RF_HFE * cos_q_RF_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_LH_foot::Type_fr_trunk_X_LH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_LH_foot& HomogeneousTransforms::Type_fr_trunk_X_LH_foot::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(0,2) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(0,3) = (- tx_LH_foot * cos_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * sin_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * sin_q_LH_HFE)+ tx_LH_HAA;
    (*this)(1,0) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(1,1) = cos_q_LH_HAA;
    (*this)(1,2) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(1,3) = ( tx_LH_foot * sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * sin_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_HFE * sin_q_LH_HAA)+ ty_LH_HAA;
    (*this)(2,0) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
    (*this)(2,1) = -sin_q_LH_HAA;
    (*this)(2,2) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
    (*this)(2,3) = ( tx_LH_foot * cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE)-( tx_LH_foot * cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( tx_LH_KFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HAA::Type_fr_trunk_X_fr_LH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(1,0) = 0.0;
    (*this)(1,1) = -1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HAA& HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HAA::update(const state_t& q)
{
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HFE::Type_fr_trunk_X_fr_LH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HFE& HomogeneousTransforms::Type_fr_trunk_X_fr_LH_HFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(1,3) =  ty_LH_HAA-( tx_LH_HFE * sin_q_LH_HAA);
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(2,3) = - tx_LH_HFE * cos_q_LH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LH_KFE::Type_fr_trunk_X_fr_LH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LH_KFE& HomogeneousTransforms::Type_fr_trunk_X_fr_LH_KFE::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = -sin_q_LH_HFE;
    (*this)(0,1) = -cos_q_LH_HFE;
    (*this)(0,3) =  tx_LH_HAA-( tx_LH_KFE * sin_q_LH_HFE);
    (*this)(1,0) = -sin_q_LH_HAA * cos_q_LH_HFE;
    (*this)(1,1) = sin_q_LH_HAA * sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HAA;
    (*this)(1,3) = (- tx_LH_KFE * sin_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_HFE * sin_q_LH_HAA)+ ty_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA * cos_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HAA * sin_q_LH_HFE;
    (*this)(2,2) = -sin_q_LH_HAA;
    (*this)(2,3) = (- tx_LH_KFE * cos_q_LH_HAA * cos_q_LH_HFE)-( tx_LH_HFE * cos_q_LH_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_RH_foot::Type_fr_trunk_X_RH_foot()
{
    (*this)(0,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_RH_foot& HomogeneousTransforms::Type_fr_trunk_X_RH_foot::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(0,2) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(0,3) = (- tx_RH_foot * cos_q_RH_HFE * sin_q_RH_KFE)-( tx_RH_foot * sin_q_RH_HFE * cos_q_RH_KFE)-( tx_RH_KFE * sin_q_RH_HFE)+ tx_RH_HAA;
    (*this)(1,0) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
    (*this)(1,3) = (- tx_RH_foot * sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)+( tx_RH_foot * sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)+( tx_RH_KFE * sin_q_RH_HAA * cos_q_RH_HFE)+( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA;
    (*this)(2,0) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
    (*this)(2,1) = sin_q_RH_HAA;
    (*this)(2,2) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
    (*this)(2,3) = ( tx_RH_foot * cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-( tx_RH_foot * cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-( tx_RH_KFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HAA::Type_fr_trunk_X_fr_RH_HAA()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_RH_HAA;    // Maxima DSL: _k__ty_RH_HAA
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HAA& HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HAA::update(const state_t& q)
{
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HFE::Type_fr_trunk_X_fr_RH_HFE()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -1.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(1,1) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HFE& HomogeneousTransforms::Type_fr_trunk_X_fr_RH_HFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(1,3) = ( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(2,3) = - tx_RH_HFE * cos_q_RH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RH_KFE::Type_fr_trunk_X_fr_RH_KFE()
{
    (*this)(0,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RH_KFE& HomogeneousTransforms::Type_fr_trunk_X_fr_RH_KFE::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = -sin_q_RH_HFE;
    (*this)(0,1) = -cos_q_RH_HFE;
    (*this)(0,3) =  tx_RH_HAA-( tx_RH_KFE * sin_q_RH_HFE);
    (*this)(1,0) = sin_q_RH_HAA * cos_q_RH_HFE;
    (*this)(1,1) = -sin_q_RH_HAA * sin_q_RH_HFE;
    (*this)(1,2) = cos_q_RH_HAA;
    (*this)(1,3) = ( tx_RH_KFE * sin_q_RH_HAA * cos_q_RH_HFE)+( tx_RH_HFE * sin_q_RH_HAA)+ ty_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA * cos_q_RH_HFE;
    (*this)(2,1) = cos_q_RH_HAA * sin_q_RH_HFE;
    (*this)(2,2) = sin_q_RH_HAA;
    (*this)(2,3) = (- tx_RH_KFE * cos_q_RH_HAA * cos_q_RH_HFE)-( tx_RH_HFE * cos_q_RH_HAA);
    return *this;
}
HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_trunk::Type_fr_LF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_trunk& HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(0,1) = -sin_q_LF_HAA;
    (*this)(0,2) = -cos_q_LF_HAA;
    (*this)(0,3) =  ty_LF_HAA * sin_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(1,2) = sin_q_LF_HAA;
    (*this)(1,3) =  ty_LF_HAA * cos_q_LF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LF_hipassembly::Type_fr_trunk_X_fr_LF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) =  tx_LF_HAA;    // Maxima DSL: _k__tx_LF_HAA
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_LF_HAA;    // Maxima DSL: _k__ty_LF_HAA
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LF_hipassembly& HomogeneousTransforms::Type_fr_trunk_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HAA  = ScalarTraits::sin( q(LF_HAA) );
    Scalar cos_q_LF_HAA  = ScalarTraits::cos( q(LF_HAA) );
    (*this)(1,0) = -sin_q_LF_HAA;
    (*this)(1,1) = -cos_q_LF_HAA;
    (*this)(2,0) = -cos_q_LF_HAA;
    (*this)(2,1) = sin_q_LF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::Type_fr_LF_upperleg_X_fr_LF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly& HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,2) = sin_q_LF_HFE;
    (*this)(0,3) = - tx_LF_HFE * cos_q_LF_HFE;
    (*this)(1,0) = -sin_q_LF_HFE;
    (*this)(1,2) = cos_q_LF_HFE;
    (*this)(1,3) =  tx_LF_HFE * sin_q_LF_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::Type_fr_LF_hipassembly_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LF_HFE;    // Maxima DSL: _k__tx_LF_HFE
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg& HomogeneousTransforms::Type_fr_LF_hipassembly_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_HFE  = ScalarTraits::sin( q(LF_HFE) );
    Scalar cos_q_LF_HFE  = ScalarTraits::cos( q(LF_HFE) );
    (*this)(0,0) = cos_q_LF_HFE;
    (*this)(0,1) = -sin_q_LF_HFE;
    (*this)(2,0) = sin_q_LF_HFE;
    (*this)(2,1) = cos_q_LF_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::Type_fr_LF_lowerleg_X_fr_LF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg& HomogeneousTransforms::Type_fr_LF_lowerleg_X_fr_LF_upperleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = sin_q_LF_KFE;
    (*this)(0,3) = - tx_LF_KFE * cos_q_LF_KFE;
    (*this)(1,0) = -sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    (*this)(1,3) =  tx_LF_KFE * sin_q_LF_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::Type_fr_LF_upperleg_X_fr_LF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LF_KFE;    // Maxima DSL: _k__tx_LF_KFE
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg& HomogeneousTransforms::Type_fr_LF_upperleg_X_fr_LF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LF_KFE  = ScalarTraits::sin( q(LF_KFE) );
    Scalar cos_q_LF_KFE  = ScalarTraits::cos( q(LF_KFE) );
    (*this)(0,0) = cos_q_LF_KFE;
    (*this)(0,1) = -sin_q_LF_KFE;
    (*this)(1,0) = sin_q_LF_KFE;
    (*this)(1,1) = cos_q_LF_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_trunk::Type_fr_RF_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = - tx_RF_HAA;    // Maxima DSL: -_k__tx_RF_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_trunk& HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(0,1) = sin_q_RF_HAA;
    (*this)(0,2) = -cos_q_RF_HAA;
    (*this)(0,3) = - ty_RF_HAA * sin_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(1,2) = sin_q_RF_HAA;
    (*this)(1,3) = - ty_RF_HAA * cos_q_RF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RF_hipassembly::Type_fr_trunk_X_fr_RF_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) =  tx_RF_HAA;    // Maxima DSL: _k__tx_RF_HAA
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_RF_HAA;    // Maxima DSL: _k__ty_RF_HAA
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RF_hipassembly& HomogeneousTransforms::Type_fr_trunk_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HAA  = ScalarTraits::sin( q(RF_HAA) );
    Scalar cos_q_RF_HAA  = ScalarTraits::cos( q(RF_HAA) );
    (*this)(1,0) = sin_q_RF_HAA;
    (*this)(1,1) = cos_q_RF_HAA;
    (*this)(2,0) = -cos_q_RF_HAA;
    (*this)(2,1) = sin_q_RF_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::Type_fr_RF_upperleg_X_fr_RF_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly& HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,2) = -sin_q_RF_HFE;
    (*this)(0,3) = - tx_RF_HFE * cos_q_RF_HFE;
    (*this)(1,0) = -sin_q_RF_HFE;
    (*this)(1,2) = -cos_q_RF_HFE;
    (*this)(1,3) =  tx_RF_HFE * sin_q_RF_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::Type_fr_RF_hipassembly_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RF_HFE;    // Maxima DSL: _k__tx_RF_HFE
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg& HomogeneousTransforms::Type_fr_RF_hipassembly_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_HFE  = ScalarTraits::sin( q(RF_HFE) );
    Scalar cos_q_RF_HFE  = ScalarTraits::cos( q(RF_HFE) );
    (*this)(0,0) = cos_q_RF_HFE;
    (*this)(0,1) = -sin_q_RF_HFE;
    (*this)(2,0) = -sin_q_RF_HFE;
    (*this)(2,1) = -cos_q_RF_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::Type_fr_RF_lowerleg_X_fr_RF_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg& HomogeneousTransforms::Type_fr_RF_lowerleg_X_fr_RF_upperleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = sin_q_RF_KFE;
    (*this)(0,3) = - tx_RF_KFE * cos_q_RF_KFE;
    (*this)(1,0) = -sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    (*this)(1,3) =  tx_RF_KFE * sin_q_RF_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::Type_fr_RF_upperleg_X_fr_RF_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RF_KFE;    // Maxima DSL: _k__tx_RF_KFE
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg& HomogeneousTransforms::Type_fr_RF_upperleg_X_fr_RF_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RF_KFE  = ScalarTraits::sin( q(RF_KFE) );
    Scalar cos_q_RF_KFE  = ScalarTraits::cos( q(RF_KFE) );
    (*this)(0,0) = cos_q_RF_KFE;
    (*this)(0,1) = -sin_q_RF_KFE;
    (*this)(1,0) = sin_q_RF_KFE;
    (*this)(1,1) = cos_q_RF_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_trunk::Type_fr_LH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = -1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_trunk& HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(0,1) = -sin_q_LH_HAA;
    (*this)(0,2) = -cos_q_LH_HAA;
    (*this)(0,3) =  ty_LH_HAA * sin_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(1,2) = sin_q_LH_HAA;
    (*this)(1,3) =  ty_LH_HAA * cos_q_LH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_LH_hipassembly::Type_fr_trunk_X_fr_LH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = -1.0;
    (*this)(0,3) =  tx_LH_HAA;    // Maxima DSL: _k__tx_LH_HAA
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_LH_HAA;    // Maxima DSL: _k__ty_LH_HAA
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_LH_hipassembly& HomogeneousTransforms::Type_fr_trunk_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HAA  = ScalarTraits::sin( q(LH_HAA) );
    Scalar cos_q_LH_HAA  = ScalarTraits::cos( q(LH_HAA) );
    (*this)(1,0) = -sin_q_LH_HAA;
    (*this)(1,1) = -cos_q_LH_HAA;
    (*this)(2,0) = -cos_q_LH_HAA;
    (*this)(2,1) = sin_q_LH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::Type_fr_LH_upperleg_X_fr_LH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly& HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,2) = sin_q_LH_HFE;
    (*this)(0,3) = - tx_LH_HFE * cos_q_LH_HFE;
    (*this)(1,0) = -sin_q_LH_HFE;
    (*this)(1,2) = cos_q_LH_HFE;
    (*this)(1,3) =  tx_LH_HFE * sin_q_LH_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::Type_fr_LH_hipassembly_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LH_HFE;    // Maxima DSL: _k__tx_LH_HFE
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg& HomogeneousTransforms::Type_fr_LH_hipassembly_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_HFE  = ScalarTraits::sin( q(LH_HFE) );
    Scalar cos_q_LH_HFE  = ScalarTraits::cos( q(LH_HFE) );
    (*this)(0,0) = cos_q_LH_HFE;
    (*this)(0,1) = -sin_q_LH_HFE;
    (*this)(2,0) = sin_q_LH_HFE;
    (*this)(2,1) = cos_q_LH_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::Type_fr_LH_lowerleg_X_fr_LH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg& HomogeneousTransforms::Type_fr_LH_lowerleg_X_fr_LH_upperleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = sin_q_LH_KFE;
    (*this)(0,3) = - tx_LH_KFE * cos_q_LH_KFE;
    (*this)(1,0) = -sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    (*this)(1,3) =  tx_LH_KFE * sin_q_LH_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::Type_fr_LH_upperleg_X_fr_LH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_LH_KFE;    // Maxima DSL: _k__tx_LH_KFE
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg& HomogeneousTransforms::Type_fr_LH_upperleg_X_fr_LH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_LH_KFE  = ScalarTraits::sin( q(LH_KFE) );
    Scalar cos_q_LH_KFE  = ScalarTraits::cos( q(LH_KFE) );
    (*this)(0,0) = cos_q_LH_KFE;
    (*this)(0,1) = -sin_q_LH_KFE;
    (*this)(1,0) = sin_q_LH_KFE;
    (*this)(1,1) = cos_q_LH_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_trunk::Type_fr_RH_hipassembly_X_fr_trunk()
{
    (*this)(0,0) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = - tx_RH_HAA;    // Maxima DSL: -_k__tx_RH_HAA
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_trunk& HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_trunk::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(0,1) = sin_q_RH_HAA;
    (*this)(0,2) = -cos_q_RH_HAA;
    (*this)(0,3) = - ty_RH_HAA * sin_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(1,2) = sin_q_RH_HAA;
    (*this)(1,3) = - ty_RH_HAA * cos_q_RH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_trunk_X_fr_RH_hipassembly::Type_fr_trunk_X_fr_RH_hipassembly()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 1.0;
    (*this)(0,3) =  tx_RH_HAA;    // Maxima DSL: _k__tx_RH_HAA
    (*this)(1,2) = 0.0;
    (*this)(1,3) =  ty_RH_HAA;    // Maxima DSL: _k__ty_RH_HAA
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_trunk_X_fr_RH_hipassembly& HomogeneousTransforms::Type_fr_trunk_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HAA  = ScalarTraits::sin( q(RH_HAA) );
    Scalar cos_q_RH_HAA  = ScalarTraits::cos( q(RH_HAA) );
    (*this)(1,0) = sin_q_RH_HAA;
    (*this)(1,1) = cos_q_RH_HAA;
    (*this)(2,0) = -cos_q_RH_HAA;
    (*this)(2,1) = sin_q_RH_HAA;
    return *this;
}
HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::Type_fr_RH_upperleg_X_fr_RH_hipassembly()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly& HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_hipassembly::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,2) = -sin_q_RH_HFE;
    (*this)(0,3) = - tx_RH_HFE * cos_q_RH_HFE;
    (*this)(1,0) = -sin_q_RH_HFE;
    (*this)(1,2) = -cos_q_RH_HFE;
    (*this)(1,3) =  tx_RH_HFE * sin_q_RH_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::Type_fr_RH_hipassembly_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RH_HFE;    // Maxima DSL: _k__tx_RH_HFE
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg& HomogeneousTransforms::Type_fr_RH_hipassembly_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_HFE  = ScalarTraits::sin( q(RH_HFE) );
    Scalar cos_q_RH_HFE  = ScalarTraits::cos( q(RH_HFE) );
    (*this)(0,0) = cos_q_RH_HFE;
    (*this)(0,1) = -sin_q_RH_HFE;
    (*this)(2,0) = -sin_q_RH_HFE;
    (*this)(2,1) = -cos_q_RH_HFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::Type_fr_RH_lowerleg_X_fr_RH_upperleg()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg& HomogeneousTransforms::Type_fr_RH_lowerleg_X_fr_RH_upperleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = sin_q_RH_KFE;
    (*this)(0,3) = - tx_RH_KFE * cos_q_RH_KFE;
    (*this)(1,0) = -sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    (*this)(1,3) =  tx_RH_KFE * sin_q_RH_KFE;
    return *this;
}
HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::Type_fr_RH_upperleg_X_fr_RH_lowerleg()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_RH_KFE;    // Maxima DSL: _k__tx_RH_KFE
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg& HomogeneousTransforms::Type_fr_RH_upperleg_X_fr_RH_lowerleg::update(const state_t& q)
{
    Scalar sin_q_RH_KFE  = ScalarTraits::sin( q(RH_KFE) );
    Scalar cos_q_RH_KFE  = ScalarTraits::cos( q(RH_KFE) );
    (*this)(0,0) = cos_q_RH_KFE;
    (*this)(0,1) = -sin_q_RH_KFE;
    (*this)(1,0) = sin_q_RH_KFE;
    (*this)(1,1) = cos_q_RH_KFE;
    return *this;
}

