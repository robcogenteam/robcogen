#ifndef RCG__HYQ_TRAITS_H_
#define RCG__HYQ_TRAITS_H_

#include "declarations.h"
#include "transforms.h"
#include "inverse_dynamics.h"
#include "forward_dynamics.h"
#include "jsim.h"
#include "inertia_properties.h"

namespace HyQ {
namespace rcg {
struct Traits {
    typedef typename HyQ::rcg::ScalarTraits ScalarTraits;

    typedef typename HyQ::rcg::JointState JointState;

    typedef typename HyQ::rcg::JointIdentifiers JointID;
    typedef typename HyQ::rcg::LinkIdentifiers  LinkID;

    typedef typename HyQ::rcg::HomogeneousTransforms HomogeneousTransforms;
    typedef typename HyQ::rcg::MotionTransforms MotionTransforms;
    typedef typename HyQ::rcg::ForceTransforms ForceTransforms;

    typedef typename HyQ::rcg::InertiaProperties InertiaProperties;
    typedef typename HyQ::rcg::ForwardDynamics FwdDynEngine;
    typedef typename HyQ::rcg::InverseDynamics InvDynEngine;
    typedef typename HyQ::rcg::JSIM JSIM;

    static const int joints_count = HyQ::rcg::jointsCount;
    static const int links_count  = HyQ::rcg::linksCount;
    static const bool floating_base = true;

    static inline const JointID* orderedJointIDs();
    static inline const LinkID*  orderedLinkIDs();
};


inline const Traits::JointID*  Traits::orderedJointIDs() {
    return HyQ::rcg::orderedJointIDs;
}
inline const Traits::LinkID*  Traits::orderedLinkIDs() {
    return HyQ::rcg::orderedLinkIDs;
}

}
}

#endif
