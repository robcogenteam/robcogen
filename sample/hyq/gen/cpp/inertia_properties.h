#ifndef RCG_HYQ_INERTIA_PROPERTIES_H_
#define RCG_HYQ_INERTIA_PROPERTIES_H_

#include <iit/rbd/rbd.h>
#include <iit/rbd/InertiaMatrix.h>
#include <iit/rbd/utils.h>

#include "declarations.h"
#include "model_constants.h"
#include "dynamics_parameters.h"

namespace HyQ {
namespace rcg {

class InertiaProperties {
    public:
        InertiaProperties();
        ~InertiaProperties();
        const InertiaMatrix& getTensor_trunk() const;
        const InertiaMatrix& getTensor_LF_hipassembly() const;
        const InertiaMatrix& getTensor_LF_upperleg() const;
        const InertiaMatrix& getTensor_LF_lowerleg() const;
        const InertiaMatrix& getTensor_RF_hipassembly() const;
        const InertiaMatrix& getTensor_RF_upperleg() const;
        const InertiaMatrix& getTensor_RF_lowerleg() const;
        const InertiaMatrix& getTensor_LH_hipassembly() const;
        const InertiaMatrix& getTensor_LH_upperleg() const;
        const InertiaMatrix& getTensor_LH_lowerleg() const;
        const InertiaMatrix& getTensor_RH_hipassembly() const;
        const InertiaMatrix& getTensor_RH_upperleg() const;
        const InertiaMatrix& getTensor_RH_lowerleg() const;
        Scalar getMass_trunk() const;
        Scalar getMass_LF_hipassembly() const;
        Scalar getMass_LF_upperleg() const;
        Scalar getMass_LF_lowerleg() const;
        Scalar getMass_RF_hipassembly() const;
        Scalar getMass_RF_upperleg() const;
        Scalar getMass_RF_lowerleg() const;
        Scalar getMass_LH_hipassembly() const;
        Scalar getMass_LH_upperleg() const;
        Scalar getMass_LH_lowerleg() const;
        Scalar getMass_RH_hipassembly() const;
        Scalar getMass_RH_upperleg() const;
        Scalar getMass_RH_lowerleg() const;
        const Vector3& getCOM_trunk() const;
        const Vector3& getCOM_LF_hipassembly() const;
        const Vector3& getCOM_LF_upperleg() const;
        const Vector3& getCOM_LF_lowerleg() const;
        const Vector3& getCOM_RF_hipassembly() const;
        const Vector3& getCOM_RF_upperleg() const;
        const Vector3& getCOM_RF_lowerleg() const;
        const Vector3& getCOM_LH_hipassembly() const;
        const Vector3& getCOM_LH_upperleg() const;
        const Vector3& getCOM_LH_lowerleg() const;
        const Vector3& getCOM_RH_hipassembly() const;
        const Vector3& getCOM_RH_upperleg() const;
        const Vector3& getCOM_RH_lowerleg() const;
        Scalar getTotalMass() const;


        /*!
         * Fresh values for the runtime parameters of the robot HyQ,
         * causing the update of the inertia properties modeled by this
         * instance.
         */
        void updateParameters(const RuntimeInertiaParams&);

    private:
        RuntimeInertiaParams params;

        InertiaMatrix tensor_trunk;
        InertiaMatrix tensor_LF_hipassembly;
        InertiaMatrix tensor_LF_upperleg;
        InertiaMatrix tensor_LF_lowerleg;
        InertiaMatrix tensor_RF_hipassembly;
        InertiaMatrix tensor_RF_upperleg;
        InertiaMatrix tensor_RF_lowerleg;
        InertiaMatrix tensor_LH_hipassembly;
        InertiaMatrix tensor_LH_upperleg;
        InertiaMatrix tensor_LH_lowerleg;
        InertiaMatrix tensor_RH_hipassembly;
        InertiaMatrix tensor_RH_upperleg;
        InertiaMatrix tensor_RH_lowerleg;
        Vector3 com_trunk;
        Vector3 com_LF_hipassembly;
        Vector3 com_LF_upperleg;
        Vector3 com_LF_lowerleg;
        Vector3 com_RF_hipassembly;
        Vector3 com_RF_upperleg;
        Vector3 com_RF_lowerleg;
        Vector3 com_LH_hipassembly;
        Vector3 com_LH_upperleg;
        Vector3 com_LH_lowerleg;
        Vector3 com_RH_hipassembly;
        Vector3 com_RH_upperleg;
        Vector3 com_RH_lowerleg;
};


inline InertiaProperties::~InertiaProperties() {}

inline const InertiaMatrix& InertiaProperties::getTensor_trunk() const {
    return this->tensor_trunk;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LF_hipassembly() const {
    return this->tensor_LF_hipassembly;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LF_upperleg() const {
    return this->tensor_LF_upperleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LF_lowerleg() const {
    return this->tensor_LF_lowerleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RF_hipassembly() const {
    return this->tensor_RF_hipassembly;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RF_upperleg() const {
    return this->tensor_RF_upperleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RF_lowerleg() const {
    return this->tensor_RF_lowerleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LH_hipassembly() const {
    return this->tensor_LH_hipassembly;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LH_upperleg() const {
    return this->tensor_LH_upperleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_LH_lowerleg() const {
    return this->tensor_LH_lowerleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RH_hipassembly() const {
    return this->tensor_RH_hipassembly;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RH_upperleg() const {
    return this->tensor_RH_upperleg;
}
inline const InertiaMatrix& InertiaProperties::getTensor_RH_lowerleg() const {
    return this->tensor_RH_lowerleg;
}
inline Scalar InertiaProperties::getMass_trunk() const {
    return this->tensor_trunk.getMass();
}
inline Scalar InertiaProperties::getMass_LF_hipassembly() const {
    return this->tensor_LF_hipassembly.getMass();
}
inline Scalar InertiaProperties::getMass_LF_upperleg() const {
    return this->tensor_LF_upperleg.getMass();
}
inline Scalar InertiaProperties::getMass_LF_lowerleg() const {
    return this->tensor_LF_lowerleg.getMass();
}
inline Scalar InertiaProperties::getMass_RF_hipassembly() const {
    return this->tensor_RF_hipassembly.getMass();
}
inline Scalar InertiaProperties::getMass_RF_upperleg() const {
    return this->tensor_RF_upperleg.getMass();
}
inline Scalar InertiaProperties::getMass_RF_lowerleg() const {
    return this->tensor_RF_lowerleg.getMass();
}
inline Scalar InertiaProperties::getMass_LH_hipassembly() const {
    return this->tensor_LH_hipassembly.getMass();
}
inline Scalar InertiaProperties::getMass_LH_upperleg() const {
    return this->tensor_LH_upperleg.getMass();
}
inline Scalar InertiaProperties::getMass_LH_lowerleg() const {
    return this->tensor_LH_lowerleg.getMass();
}
inline Scalar InertiaProperties::getMass_RH_hipassembly() const {
    return this->tensor_RH_hipassembly.getMass();
}
inline Scalar InertiaProperties::getMass_RH_upperleg() const {
    return this->tensor_RH_upperleg.getMass();
}
inline Scalar InertiaProperties::getMass_RH_lowerleg() const {
    return this->tensor_RH_lowerleg.getMass();
}
inline const Vector3& InertiaProperties::getCOM_trunk() const {
    return this->com_trunk;
}
inline const Vector3& InertiaProperties::getCOM_LF_hipassembly() const {
    return this->com_LF_hipassembly;
}
inline const Vector3& InertiaProperties::getCOM_LF_upperleg() const {
    return this->com_LF_upperleg;
}
inline const Vector3& InertiaProperties::getCOM_LF_lowerleg() const {
    return this->com_LF_lowerleg;
}
inline const Vector3& InertiaProperties::getCOM_RF_hipassembly() const {
    return this->com_RF_hipassembly;
}
inline const Vector3& InertiaProperties::getCOM_RF_upperleg() const {
    return this->com_RF_upperleg;
}
inline const Vector3& InertiaProperties::getCOM_RF_lowerleg() const {
    return this->com_RF_lowerleg;
}
inline const Vector3& InertiaProperties::getCOM_LH_hipassembly() const {
    return this->com_LH_hipassembly;
}
inline const Vector3& InertiaProperties::getCOM_LH_upperleg() const {
    return this->com_LH_upperleg;
}
inline const Vector3& InertiaProperties::getCOM_LH_lowerleg() const {
    return this->com_LH_lowerleg;
}
inline const Vector3& InertiaProperties::getCOM_RH_hipassembly() const {
    return this->com_RH_hipassembly;
}
inline const Vector3& InertiaProperties::getCOM_RH_upperleg() const {
    return this->com_RH_upperleg;
}
inline const Vector3& InertiaProperties::getCOM_RH_lowerleg() const {
    return this->com_RH_lowerleg;
}

inline Scalar InertiaProperties::getTotalMass() const {
    return m_trunk + m_LF_hipassembly + m_LF_upperleg + m_LF_lowerleg + m_RF_hipassembly + m_RF_upperleg + m_RF_lowerleg + m_LH_hipassembly + m_LH_upperleg + m_LH_lowerleg + m_RH_hipassembly + m_RH_upperleg + m_RH_lowerleg;
}

}
}

#endif
