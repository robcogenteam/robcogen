#include <iit/robcogen/test/dynamics_consistency.h>

#include <HyQ/rcg/inertia_properties.h>
#include <HyQ/rcg/transforms.h>
#include <HyQ/rcg/traits.h>
#include <HyQ/rcg/kinematics_parameters.h>
#include <HyQ/rcg/dynamics_parameters.h>

using namespace HyQ::rcg;

/**
 * This program calls the dynamics-consistency-test implemented in
 * iit::robcogen::test.
 *
 * No arguments are required, all the relevant joint-space quantities are
 * randomly generated.
 *
 * The test prints some output on stdout. All the numerical values should be
 * zero; if that is not the case, there is some inconsistency among the generated
 * dynamics algorithms.
 */
int main(int argc, char** argv)
{
    MotionTransforms xm;
    ForceTransforms  xf;
    InertiaProperties ip;

    iit::robcogen::test::consistencyTests<Traits>(ip, xm, xf);

    return 0;
}
