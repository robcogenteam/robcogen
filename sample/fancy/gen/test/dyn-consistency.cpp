#include <iit/robcogen/test/dynamics_consistency.h>

#include <Fancy/rcg/inertia_properties.h>
#include <Fancy/rcg/transforms.h>
#include <Fancy/rcg/traits.h>
#include <Fancy/rcg/kinematics_parameters.h>
#include <Fancy/rcg/dynamics_parameters.h>

using namespace Fancy::rcg;

/**
 * This program calls the dynamics-consistency-test implemented in
 * iit::robcogen::test.
 *
 * No arguments are required, all the relevant joint-space quantities are
 * randomly generated.
 *
 * The test prints some output on stdout. All the numerical values should be
 * zero; if that is not the case, there is some inconsistency among the generated
 * dynamics algorithms.
 */
int main(int argc, char** argv)
{
    MotionTransforms xm;
    ForceTransforms  xf;
    InertiaProperties ip;

    iit::robcogen::test::consistencyTests<Traits>(ip, xm, xf);

    return 0;
}
