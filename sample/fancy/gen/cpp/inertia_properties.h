#ifndef RCG_FANCY_INERTIA_PROPERTIES_H_
#define RCG_FANCY_INERTIA_PROPERTIES_H_

#include <iit/rbd/rbd.h>
#include <iit/rbd/InertiaMatrix.h>
#include <iit/rbd/utils.h>

#include "declarations.h"
#include "model_constants.h"
#include "dynamics_parameters.h"

namespace Fancy {
namespace rcg {

class InertiaProperties {
    public:
        InertiaProperties();
        ~InertiaProperties();
        const InertiaMatrix& getTensor_link1() const;
        const InertiaMatrix& getTensor_link2() const;
        const InertiaMatrix& getTensor_link3() const;
        const InertiaMatrix& getTensor_link4() const;
        const InertiaMatrix& getTensor_link5() const;
        Scalar getMass_link1() const;
        Scalar getMass_link2() const;
        Scalar getMass_link3() const;
        Scalar getMass_link4() const;
        Scalar getMass_link5() const;
        const Vector3& getCOM_link1() const;
        const Vector3& getCOM_link2() const;
        const Vector3& getCOM_link3() const;
        const Vector3& getCOM_link4() const;
        const Vector3& getCOM_link5() const;
        Scalar getTotalMass() const;


        /*!
         * Fresh values for the runtime parameters of the robot Fancy,
         * causing the update of the inertia properties modeled by this
         * instance.
         */
        void updateParameters(const RuntimeInertiaParams&);

    private:
        RuntimeInertiaParams params;

        InertiaMatrix tensor_link1;
        InertiaMatrix tensor_link2;
        InertiaMatrix tensor_link3;
        InertiaMatrix tensor_link4;
        InertiaMatrix tensor_link5;
        Vector3 com_link1;
        Vector3 com_link2;
        Vector3 com_link3;
        Vector3 com_link4;
        Vector3 com_link5;
};


inline InertiaProperties::~InertiaProperties() {}

inline const InertiaMatrix& InertiaProperties::getTensor_link1() const {
    return this->tensor_link1;
}
inline const InertiaMatrix& InertiaProperties::getTensor_link2() const {
    return this->tensor_link2;
}
inline const InertiaMatrix& InertiaProperties::getTensor_link3() const {
    return this->tensor_link3;
}
inline const InertiaMatrix& InertiaProperties::getTensor_link4() const {
    return this->tensor_link4;
}
inline const InertiaMatrix& InertiaProperties::getTensor_link5() const {
    return this->tensor_link5;
}
inline Scalar InertiaProperties::getMass_link1() const {
    return this->tensor_link1.getMass();
}
inline Scalar InertiaProperties::getMass_link2() const {
    return this->tensor_link2.getMass();
}
inline Scalar InertiaProperties::getMass_link3() const {
    return this->tensor_link3.getMass();
}
inline Scalar InertiaProperties::getMass_link4() const {
    return this->tensor_link4.getMass();
}
inline Scalar InertiaProperties::getMass_link5() const {
    return this->tensor_link5.getMass();
}
inline const Vector3& InertiaProperties::getCOM_link1() const {
    return this->com_link1;
}
inline const Vector3& InertiaProperties::getCOM_link2() const {
    return this->com_link2;
}
inline const Vector3& InertiaProperties::getCOM_link3() const {
    return this->com_link3;
}
inline const Vector3& InertiaProperties::getCOM_link4() const {
    return this->com_link4;
}
inline const Vector3& InertiaProperties::getCOM_link5() const {
    return this->com_link5;
}

inline Scalar InertiaProperties::getTotalMass() const {
    return m_link1 + m_link2 + m_link3 + m_link4 + m_link5;
}

}
}

#endif
