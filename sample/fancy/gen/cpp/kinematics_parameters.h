#ifndef _FANCY_PARAMETERS_DEFS_
#define _FANCY_PARAMETERS_DEFS_

#include "rbd_types.h"

namespace Fancy {
namespace rcg {

struct Params_lengths {
    Params_lengths() {
        defaults();
    }
    void defaults() {
    }
};

struct Params_angles {
    Params_angles() {
        defaults();
    }
    void defaults() {
    }
};


}
}
#endif
