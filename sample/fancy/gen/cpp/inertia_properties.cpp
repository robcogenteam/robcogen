#include "inertia_properties.h"

using namespace std;
using namespace iit::rbd;

Fancy::rcg::InertiaProperties::InertiaProperties()
{
    com_link1 = Vector3(comx_link1,0.0,0.0);
    tensor_link1.fill(
        m_link1,
        com_link1,
        Utils::buildInertiaTensor<Scalar>(ix_link1,iy_link1,iz_link1,0.0,0.0,0.0) );

    com_link2 = Vector3(0.0,0.0,comz_link2);
    tensor_link2.fill(
        m_link2,
        com_link2,
        Utils::buildInertiaTensor<Scalar>(ix_link2,iy_link2,iz_link2,0.0,0.0,0.0) );

    com_link3 = Vector3(comx_link3,0.0,0.0);
    tensor_link3.fill(
        m_link3,
        com_link3,
        Utils::buildInertiaTensor<Scalar>(ix_link3,iy_link3,iz_link3,0.0,0.0,0.0) );

    com_link4 = Vector3(0.0,0.0,comz_link4);
    tensor_link4.fill(
        m_link4,
        com_link4,
        Utils::buildInertiaTensor<Scalar>(ix_link4,iy_link4,iz_link4,0.0,0.0,0.0) );

    com_link5 = Vector3(comx_link5,0.0,0.0);
    tensor_link5.fill(
        m_link5,
        com_link5,
        Utils::buildInertiaTensor<Scalar>(ix_link5,iy_link5,iz_link5,0.0,0.0,0.0) );

}


void Fancy::rcg::InertiaProperties::updateParameters(const RuntimeInertiaParams& fresh)
{
    this-> params = fresh;
}
