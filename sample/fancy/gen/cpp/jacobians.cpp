#include "jacobians.h"

Fancy::rcg::Jacobians::Jacobians()
:    fr_base0_J_ee()
{}

void Fancy::rcg::Jacobians::updateParameters(const Params_lengths& _lengths, const Params_angles& _angles)
{
    params.lengths = _lengths;
    params.angles = _angles;
    params.trig.update();
}

Fancy::rcg::Jacobians::Type_fr_base0_J_ee::Type_fr_base0_J_ee()
{
    (*this)(0,0) = 0.0;
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 1.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
}

const Fancy::rcg::Jacobians::Type_fr_base0_J_ee& Fancy::rcg::Jacobians::Type_fr_base0_J_ee::update(const JointState& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,2) = -sin_q_jA;
    (*this)(0,4) = cos_q_jA * sin_q_jC;
    (*this)(1,2) = cos_q_jA;
    (*this)(1,4) = sin_q_jA * sin_q_jC;
    (*this)(2,4) = cos_q_jC;
    (*this)(3,0) = (- tx_ee * cos_q_jA * sin_q_jE)-( tx_ee * sin_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jC *  q(JD))-( tz_jE * sin_q_jA * sin_q_jC)-( tx_jD * sin_q_jA * cos_q_jC)-(cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA);
    (*this)(3,1) = -sin_q_jA;
    (*this)(3,2) = (- tx_ee * cos_q_jA * sin_q_jC * cos_q_jE)+(cos_q_jA * cos_q_jC *  q(JD))-( tx_jD * cos_q_jA * sin_q_jC)+( tz_jE * cos_q_jA * cos_q_jC);
    (*this)(3,3) = cos_q_jA * sin_q_jC;
    (*this)(3,4) = (- tx_ee * cos_q_jA * cos_q_jC * sin_q_jE)-( tx_ee * sin_q_jA * cos_q_jE);
    (*this)(4,0) = (- tx_ee * sin_q_jA * sin_q_jE)+( tx_ee * cos_q_jA * cos_q_jC * cos_q_jE)+(cos_q_jA * sin_q_jC *  q(JD))+( tz_jE * cos_q_jA * sin_q_jC)+( tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(4,1) = cos_q_jA;
    (*this)(4,2) = (- tx_ee * sin_q_jA * sin_q_jC * cos_q_jE)+(sin_q_jA * cos_q_jC *  q(JD))-( tx_jD * sin_q_jA * sin_q_jC)+( tz_jE * sin_q_jA * cos_q_jC);
    (*this)(4,3) = sin_q_jA * sin_q_jC;
    (*this)(4,4) = ( tx_ee * cos_q_jA * cos_q_jE)-( tx_ee * sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(5,2) = (- tx_ee * cos_q_jC * cos_q_jE)-(sin_q_jC *  q(JD))-( tz_jE * sin_q_jC)-( tx_jD * cos_q_jC);
    (*this)(5,3) = cos_q_jC;
    (*this)(5,4) =  tx_ee * sin_q_jC * sin_q_jE;
    return *this;
}

