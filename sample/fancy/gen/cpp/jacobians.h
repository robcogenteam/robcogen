#ifndef FANCY_JACOBIANS_H_
#define FANCY_JACOBIANS_H_

#include <iit/rbd/TransformsBase.h>
#include "declarations.h"
#include "kinematics_parameters.h"
#include "transforms.h" // to use the same 'Parameters' struct defined there
#include "model_constants.h"

namespace Fancy {
namespace rcg {

template<int COLS, class M>
class JacobianT : public iit::rbd::JacobianBase<JointState, COLS, M>
{};

/**
 *
 */
class Jacobians
{
    public:
        
        struct Type_fr_base0_J_ee : public JacobianT<5, Type_fr_base0_J_ee>
        {
            Type_fr_base0_J_ee();
            const Type_fr_base0_J_ee& update(const JointState&);
        };
        
    public:
        Jacobians();
        void updateParameters(const Params_lengths& _lengths, const Params_angles& _angles);
    public:
        Type_fr_base0_J_ee fr_base0_J_ee;

    protected:
        Parameters params;

};


}
}

#endif
