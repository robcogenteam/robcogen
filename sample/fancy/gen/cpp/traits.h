#ifndef RCG__FANCY_TRAITS_H_
#define RCG__FANCY_TRAITS_H_

#include "declarations.h"
#include "transforms.h"
#include "inverse_dynamics.h"
#include "forward_dynamics.h"
#include "jsim.h"
#include "inertia_properties.h"

namespace Fancy {
namespace rcg {
struct Traits {
    typedef typename Fancy::rcg::ScalarTraits ScalarTraits;

    typedef typename Fancy::rcg::JointState JointState;

    typedef typename Fancy::rcg::JointIdentifiers JointID;
    typedef typename Fancy::rcg::LinkIdentifiers  LinkID;

    typedef typename Fancy::rcg::HomogeneousTransforms HomogeneousTransforms;
    typedef typename Fancy::rcg::MotionTransforms MotionTransforms;
    typedef typename Fancy::rcg::ForceTransforms ForceTransforms;

    typedef typename Fancy::rcg::InertiaProperties InertiaProperties;
    typedef typename Fancy::rcg::ForwardDynamics FwdDynEngine;
    typedef typename Fancy::rcg::InverseDynamics InvDynEngine;
    typedef typename Fancy::rcg::JSIM JSIM;

    static const int joints_count = Fancy::rcg::jointsCount;
    static const int links_count  = Fancy::rcg::linksCount;
    static const bool floating_base = false;

    static inline const JointID* orderedJointIDs();
    static inline const LinkID*  orderedLinkIDs();
};


inline const Traits::JointID*  Traits::orderedJointIDs() {
    return Fancy::rcg::orderedJointIDs;
}
inline const Traits::LinkID*  Traits::orderedLinkIDs() {
    return Fancy::rcg::orderedLinkIDs;
}

}
}

#endif
