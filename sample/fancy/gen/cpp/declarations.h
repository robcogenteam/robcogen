#ifndef RCG_FANCY_DECLARATIONS_H_
#define RCG_FANCY_DECLARATIONS_H_

#include "rbd_types.h"

namespace Fancy {
namespace rcg {

static constexpr int JointSpaceDimension = 5;
static constexpr int jointsCount = 5;
/** The total number of rigid bodies of this robot, including the base */
static constexpr int linksCount  = 6;

typedef Matrix<5, 1> Column5d;
typedef Column5d JointState;

enum JointIdentifiers {
    JA = 0
    , JB
    , JC
    , JD
    , JE
};

enum LinkIdentifiers {
    BASE0 = 0
    , LINK1
    , LINK2
    , LINK3
    , LINK4
    , LINK5
};

static const JointIdentifiers orderedJointIDs[jointsCount] =
    {JA,JB,JC,JD,JE};

static const LinkIdentifiers orderedLinkIDs[linksCount] =
    {BASE0,LINK1,LINK2,LINK3,LINK4,LINK5};

}
}
#endif
