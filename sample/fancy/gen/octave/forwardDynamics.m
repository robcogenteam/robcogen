function qdd = forwardDynamics(ip, xm, qd, tau, fext)
g = 9.81;
qdd = zeros(5,1);
link1_v = zeros(6,1);
link1_AI = ip.lf_link1.tensor6D;
link2_AI = ip.lf_link2.tensor6D;
link3_AI = ip.lf_link3.tensor6D;
link4_AI = ip.lf_link4.tensor6D;
link5_AI = ip.lf_link5.tensor6D;
if nargin > 4
    link1_p = - fext{1};
    link2_p = - fext{2};
    link3_p = - fext{3};
    link4_p = - fext{4};
    link5_p = - fext{5};
else
    link1_p = zeros(6,1);
    link2_p = zeros(6,1);
    link3_p = zeros(6,1);
    link4_p = zeros(6,1);
    link5_p = zeros(6,1);
end
%% ---------------------- FIRST PASS ----------------------
%% Note that, during the first pass, the articulated inertias are really
%% just the spatial inertia of the links (see assignments above).
%%  Afterwards things change, and articulated inertias shall not be used
%%  in functions which work specifically with spatial inertias.

link1_v(3) = qd(1);

link1_p = link1_p + vxIv(qd(1), link1_AI);

link2_v = (xm.fr_link2_XM_fr_link1) * link1_v;
link2_v(6) = link2_v(6) + qd(2);

vcross = vcross_mx(link2_v);
link2_c = vcross(:,6) * qd(2);
link2_p = link2_p + -vcross' * link2_AI * link2_v; %%% vxIv(link2_v, link2_AI);

link3_v = (xm.fr_link3_XM_fr_link2) * link2_v;
link3_v(3) = link3_v(3) + qd(3);

vcross = vcross_mx(link3_v);
link3_c = vcross(:,3) * qd(3);
link3_p = link3_p + -vcross' * link3_AI * link3_v; %%% vxIv(link3_v, link3_AI);

link4_v = (xm.fr_link4_XM_fr_link3) * link3_v;
link4_v(6) = link4_v(6) + qd(4);

vcross = vcross_mx(link4_v);
link4_c = vcross(:,6) * qd(4);
link4_p = link4_p + -vcross' * link4_AI * link4_v; %%% vxIv(link4_v, link4_AI);

link5_v = (xm.fr_link5_XM_fr_link4) * link4_v;
link5_v(3) = link5_v(3) + qd(5);

vcross = vcross_mx(link5_v);
link5_c = vcross(:,3) * qd(5);
link5_p = link5_p + -vcross' * link5_AI * link5_v; %%% vxIv(link5_v, link5_AI);


%% ---------------------- SECOND PASS ----------------------
IaB = zeros(6,6);
pa  = zeros(6,1);

link5_u = tau(5) - link5_p(3);
link5_U = link5_AI(:,3);
link5_D = link5_U(3);

Ia_r = link5_AI - link5_U/link5_D * link5_U';
pa = link5_p + Ia_r * link5_c + link5_U * link5_u/link5_D;
IaB = xm.fr_link5_XM_fr_link4' * Ia_r * xm.fr_link5_XM_fr_link4;          %%ctransform_Ia_revolute(Ia_r, xm.fr_link5_XM_fr_link4, IaB);
link4_AI = link4_AI + IaB;
link4_p = link4_p + (xm.fr_link5_XM_fr_link4)' * pa;

link4_u = tau(4) - link4_p(6);
link4_U = link4_AI(:,6);
link4_D = link4_U(6);

Ia_p = link4_AI - link4_U/link4_D * link4_U';
pa = link4_p + Ia_p * link4_c + link4_U * link4_u/link4_D;
IaB = xm.fr_link4_XM_fr_link3' * Ia_p * xm.fr_link4_XM_fr_link3;          %% ctransform_Ia_prismatic(Ia_p, xm.fr_link4_XM_fr_link3, IaB);
link3_AI = link3_AI + IaB;
link3_p = link3_p + (xm.fr_link4_XM_fr_link3)' * pa;

link3_u = tau(3) - link3_p(3);
link3_U = link3_AI(:,3);
link3_D = link3_U(3);

Ia_r = link3_AI - link3_U/link3_D * link3_U';
pa = link3_p + Ia_r * link3_c + link3_U * link3_u/link3_D;
IaB = xm.fr_link3_XM_fr_link2' * Ia_r * xm.fr_link3_XM_fr_link2;          %%ctransform_Ia_revolute(Ia_r, xm.fr_link3_XM_fr_link2, IaB);
link2_AI = link2_AI + IaB;
link2_p = link2_p + (xm.fr_link3_XM_fr_link2)' * pa;

link2_u = tau(2) - link2_p(6);
link2_U = link2_AI(:,6);
link2_D = link2_U(6);

Ia_p = link2_AI - link2_U/link2_D * link2_U';
pa = link2_p + Ia_p * link2_c + link2_U * link2_u/link2_D;
IaB = xm.fr_link2_XM_fr_link1' * Ia_p * xm.fr_link2_XM_fr_link1;          %% ctransform_Ia_prismatic(Ia_p, xm.fr_link2_XM_fr_link1, IaB);
link1_AI = link1_AI + IaB;
link1_p = link1_p + (xm.fr_link2_XM_fr_link1)' * pa;

link1_u = tau(1) - link1_p(3);
link1_U = link1_AI(:,3);
link1_D = link1_U(3);



% ---------------------- THIRD PASS ----------------------
link1_a = (xm.fr_link1_XM_fr_base0)(:,6) * g;  % (:,6) = column of linear Z
qdd(1) = (link1_u - dot(link1_U,link1_a)) / link1_D;
link1_a(3) = link1_a(3) + qdd(1);
link2_a = (xm.fr_link2_XM_fr_link1) * link1_a + link2_c;
qdd(2) = (link2_u - dot(link2_U,link2_a)) / link2_D;
link2_a(6) = link2_a(6) + qdd(2);
link3_a = (xm.fr_link3_XM_fr_link2) * link2_a + link3_c;
qdd(3) = (link3_u - dot(link3_U,link3_a)) / link3_D;
link3_a(3) = link3_a(3) + qdd(3);
link4_a = (xm.fr_link4_XM_fr_link3) * link3_a + link4_c;
qdd(4) = (link4_u - dot(link4_U,link4_a)) / link4_D;
link4_a(6) = link4_a(6) + qdd(4);
link5_a = (xm.fr_link5_XM_fr_link4) * link4_a + link5_c;
qdd(5) = (link5_u - dot(link5_U,link5_a)) / link5_D;
link5_a(3) = link5_a(3) + qdd(5);

end

function ret = vxIv(omegaz, I)
    wz2 = omegaz*omegaz;
    ret = zeros(6,1);
    ret(1) = -I(2,3) * wz2;
    ret(2) =  I(1,2) * wz2;
    %%ret(3) =  0;
    ret(4) =  I(2,6) * wz2;
    ret(5) =  I(3,4) * wz2;
    %%ret(6) =  0;
end

function vc = vcross_mx(v)
    vc = [   0    -v(3)  v(2)   0     0     0    ;
             v(3)  0    -v(1)   0     0     0    ;
            -v(2)  v(1)  0      0     0     0    ;
             0    -v(6)  v(5)   0    -v(3)  v(2) ;
             v(6)  0    -v(4)   v(3)  0    -v(1) ;
            -v(5)  v(4)  0     -v(2)  v(1)  0    ];
end
