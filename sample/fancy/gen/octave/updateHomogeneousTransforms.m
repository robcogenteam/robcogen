function out = updateHomogeneousTransforms(tr, q, params, consts)

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
sin_q_jC = sin( q(3));
cos_q_jC = cos( q(3));
sin_q_jE = sin( q(5));
cos_q_jE = cos( q(5));
tr.fr_base0_Xh_ee(1,1) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
tr.fr_base0_Xh_ee(1,2) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
tr.fr_base0_Xh_ee(1,3) = cos_q_jA * sin_q_jC;
tr.fr_base0_Xh_ee(1,4) = (- consts.tx_ee * sin_q_jA * sin_q_jE)+( consts.tx_ee * cos_q_jA * cos_q_jC * cos_q_jE)+(cos_q_jA * sin_q_jC *  q(4))+( consts.tz_jE * cos_q_jA * sin_q_jC)+( consts.tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(2))-( consts.tz_jC * sin_q_jA)+( consts.tx_jB * cos_q_jA);
tr.fr_base0_Xh_ee(2,1) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
tr.fr_base0_Xh_ee(2,2) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
tr.fr_base0_Xh_ee(2,3) = sin_q_jA * sin_q_jC;
tr.fr_base0_Xh_ee(2,4) = ( consts.tx_ee * cos_q_jA * sin_q_jE)+( consts.tx_ee * sin_q_jA * cos_q_jC * cos_q_jE)+(sin_q_jA * sin_q_jC *  q(4))+( consts.tz_jE * sin_q_jA * sin_q_jC)+( consts.tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(2))+( consts.tx_jB * sin_q_jA)+( consts.tz_jC * cos_q_jA);
tr.fr_base0_Xh_ee(3,1) = -sin_q_jC * cos_q_jE;
tr.fr_base0_Xh_ee(3,2) = sin_q_jC * sin_q_jE;
tr.fr_base0_Xh_ee(3,3) = cos_q_jC;
tr.fr_base0_Xh_ee(3,4) = (- consts.tx_ee * sin_q_jC * cos_q_jE)+(cos_q_jC *  q(4))-( consts.tx_jD * sin_q_jC)+( consts.tz_jE * cos_q_jC);

sin_q_jE = sin( q(5));
cos_q_jE = cos( q(5));
tr.fr_jD_Xh_ee(1,1) = cos_q_jE;
tr.fr_jD_Xh_ee(1,2) = -sin_q_jE;
tr.fr_jD_Xh_ee(1,4) =  consts.tx_ee * cos_q_jE;
tr.fr_jD_Xh_ee(2,1) = sin_q_jE;
tr.fr_jD_Xh_ee(2,2) = cos_q_jE;
tr.fr_jD_Xh_ee(2,4) =  consts.tx_ee * sin_q_jE;
tr.fr_jD_Xh_ee(3,4) =  q(4)+ consts.tz_jE;


sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
tr.fr_base0_Xh_fr_jB(1,1) = cos_q_jA;
tr.fr_base0_Xh_fr_jB(1,3) = -sin_q_jA;
tr.fr_base0_Xh_fr_jB(1,4) =  consts.tx_jB * cos_q_jA;
tr.fr_base0_Xh_fr_jB(2,1) = sin_q_jA;
tr.fr_base0_Xh_fr_jB(2,3) = cos_q_jA;
tr.fr_base0_Xh_fr_jB(2,4) =  consts.tx_jB * sin_q_jA;

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
tr.fr_base0_Xh_fr_jC(1,1) = cos_q_jA;
tr.fr_base0_Xh_fr_jC(1,3) = -sin_q_jA;
tr.fr_base0_Xh_fr_jC(1,4) = (-sin_q_jA *  q(2))-( consts.tz_jC * sin_q_jA)+( consts.tx_jB * cos_q_jA);
tr.fr_base0_Xh_fr_jC(2,1) = sin_q_jA;
tr.fr_base0_Xh_fr_jC(2,3) = cos_q_jA;
tr.fr_base0_Xh_fr_jC(2,4) = (cos_q_jA *  q(2))+( consts.tx_jB * sin_q_jA)+( consts.tz_jC * cos_q_jA);

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
sin_q_jC = sin( q(3));
cos_q_jC = cos( q(3));
tr.fr_base0_Xh_fr_jD(1,1) = cos_q_jA * cos_q_jC;
tr.fr_base0_Xh_fr_jD(1,2) = -sin_q_jA;
tr.fr_base0_Xh_fr_jD(1,3) = cos_q_jA * sin_q_jC;
tr.fr_base0_Xh_fr_jD(1,4) = ( consts.tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(2))-( consts.tz_jC * sin_q_jA)+( consts.tx_jB * cos_q_jA);
tr.fr_base0_Xh_fr_jD(2,1) = sin_q_jA * cos_q_jC;
tr.fr_base0_Xh_fr_jD(2,2) = cos_q_jA;
tr.fr_base0_Xh_fr_jD(2,3) = sin_q_jA * sin_q_jC;
tr.fr_base0_Xh_fr_jD(2,4) = ( consts.tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(2))+( consts.tx_jB * sin_q_jA)+( consts.tz_jC * cos_q_jA);
tr.fr_base0_Xh_fr_jD(3,1) = -sin_q_jC;
tr.fr_base0_Xh_fr_jD(3,3) = cos_q_jC;
tr.fr_base0_Xh_fr_jD(3,4) = - consts.tx_jD * sin_q_jC;

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
sin_q_jC = sin( q(3));
cos_q_jC = cos( q(3));
tr.fr_base0_Xh_fr_jE(1,1) = cos_q_jA * cos_q_jC;
tr.fr_base0_Xh_fr_jE(1,2) = -sin_q_jA;
tr.fr_base0_Xh_fr_jE(1,3) = cos_q_jA * sin_q_jC;
tr.fr_base0_Xh_fr_jE(1,4) = (cos_q_jA * sin_q_jC *  q(4))+( consts.tz_jE * cos_q_jA * sin_q_jC)+( consts.tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(2))-( consts.tz_jC * sin_q_jA)+( consts.tx_jB * cos_q_jA);
tr.fr_base0_Xh_fr_jE(2,1) = sin_q_jA * cos_q_jC;
tr.fr_base0_Xh_fr_jE(2,2) = cos_q_jA;
tr.fr_base0_Xh_fr_jE(2,3) = sin_q_jA * sin_q_jC;
tr.fr_base0_Xh_fr_jE(2,4) = (sin_q_jA * sin_q_jC *  q(4))+( consts.tz_jE * sin_q_jA * sin_q_jC)+( consts.tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(2))+( consts.tx_jB * sin_q_jA)+( consts.tz_jC * cos_q_jA);
tr.fr_base0_Xh_fr_jE(3,1) = -sin_q_jC;
tr.fr_base0_Xh_fr_jE(3,3) = cos_q_jC;
tr.fr_base0_Xh_fr_jE(3,4) = (cos_q_jC *  q(4))-( consts.tx_jD * sin_q_jC)+( consts.tz_jE * cos_q_jC);

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
tr.fr_link1_Xh_fr_base0(1,1) = cos_q_jA;
tr.fr_link1_Xh_fr_base0(1,2) = sin_q_jA;
tr.fr_link1_Xh_fr_base0(2,1) = -sin_q_jA;
tr.fr_link1_Xh_fr_base0(2,2) = cos_q_jA;

sin_q_jA = sin( q(1));
cos_q_jA = cos( q(1));
tr.fr_base0_Xh_fr_link1(1,1) = cos_q_jA;
tr.fr_base0_Xh_fr_link1(1,2) = -sin_q_jA;
tr.fr_base0_Xh_fr_link1(2,1) = sin_q_jA;
tr.fr_base0_Xh_fr_link1(2,2) = cos_q_jA;

tr.fr_link2_Xh_fr_link1(3,4) = - q(2);

tr.fr_link1_Xh_fr_link2(2,4) =  q(2);

sin_q_jC = sin( q(3));
cos_q_jC = cos( q(3));
tr.fr_link3_Xh_fr_link2(1,1) = cos_q_jC;
tr.fr_link3_Xh_fr_link2(1,2) = sin_q_jC;
tr.fr_link3_Xh_fr_link2(2,1) = -sin_q_jC;
tr.fr_link3_Xh_fr_link2(2,2) = cos_q_jC;

sin_q_jC = sin( q(3));
cos_q_jC = cos( q(3));
tr.fr_link2_Xh_fr_link3(1,1) = cos_q_jC;
tr.fr_link2_Xh_fr_link3(1,2) = -sin_q_jC;
tr.fr_link2_Xh_fr_link3(2,1) = sin_q_jC;
tr.fr_link2_Xh_fr_link3(2,2) = cos_q_jC;

tr.fr_link4_Xh_fr_link3(3,4) = - q(4);

tr.fr_link3_Xh_fr_link4(2,4) = - q(4);

sin_q_jE = sin( q(5));
cos_q_jE = cos( q(5));
tr.fr_link5_Xh_fr_link4(1,1) = cos_q_jE;
tr.fr_link5_Xh_fr_link4(1,2) = sin_q_jE;
tr.fr_link5_Xh_fr_link4(2,1) = -sin_q_jE;
tr.fr_link5_Xh_fr_link4(2,2) = cos_q_jE;

sin_q_jE = sin( q(5));
cos_q_jE = cos( q(5));
tr.fr_link4_Xh_fr_link5(1,1) = cos_q_jE;
tr.fr_link4_Xh_fr_link5(1,2) = -sin_q_jE;
tr.fr_link4_Xh_fr_link5(2,1) = sin_q_jE;
tr.fr_link4_Xh_fr_link5(2,2) = cos_q_jE;



out = tr;