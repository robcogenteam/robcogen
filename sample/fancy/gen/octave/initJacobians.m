function jacs = initJacobians(consts)

jacs.fr_base0_J_ee = zeros(6,5);
jacs.fr_base0_J_ee(1,1) = 0.0;
jacs.fr_base0_J_ee(1,2) = 0.0;
jacs.fr_base0_J_ee(1,4) = 0.0;
jacs.fr_base0_J_ee(2,1) = 0.0;
jacs.fr_base0_J_ee(2,2) = 0.0;
jacs.fr_base0_J_ee(2,4) = 0.0;
jacs.fr_base0_J_ee(3,1) = 1.0;
jacs.fr_base0_J_ee(3,2) = 0.0;
jacs.fr_base0_J_ee(3,3) = 0.0;
jacs.fr_base0_J_ee(3,4) = 0.0;
jacs.fr_base0_J_ee(6,1) = 0.0;
jacs.fr_base0_J_ee(6,2) = 0.0;

