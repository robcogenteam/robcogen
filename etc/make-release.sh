#!/bin/sh

#TODO check for clean git tree before continuing

if [ "$#" -ne 1 ]; then
    echo "Please provide the version number"
    exit
fi

VER=$1

make release-purge release     \
&& cp etc/release/* release/   \
&& echo $VER > release/version \
&& git tag -a "v$VER" --message="Released version $VER"
