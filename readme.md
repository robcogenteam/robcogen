# Introduction

This repository contains all the packages which together form the codebase
of RobCoGen, the Robotics Code Generator.
Every package is a git-submodule of *this* git repository.

To clone the entire RobCoGen codebase please git-clone this repository and
then initialize the submodules with:

```
    git submodule update --init
```

More information about what RobCoGen is, and what it can do, can be found in the
[website](http://robcogenteam.bitbucket.io).




# Copyright notice

The RobCoGen project.

Copyright © 2015-2020, Marco Frigerio
all rights reserved.

See the license file for additional information.
