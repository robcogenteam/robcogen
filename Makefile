
# Root of the build sub-system of the tool and extra-resources directory.
# These *must match* the actual folders in the tree.
#
dir_building := building


# Name of the folder for the results of compilation, and for the release
# These are *configuration choices* of this Makefile
#
dir_binaries  := 0build



# Actual path where the building-output will be found
#
dir_build_out := $(dir_building)/$(dir_binaries)


# RSYNC script (add '--verbose --dry-run' for debugging)
#
cmd_sync := rsync --perms --times --recursive --exclude='/*gen' --exclude='build/' --exclude='.*' --exclude='.git' --exclude='*.log'


#
# TARGETS
#

all : dobuild

### Building
# Recursively invoke Make in the building subdirectory.
# Make sure to create the required symlinks, if necessary (see the readme file
#   in the building subfolder).
#

dsls     := dsls/kinematics dsls/motions dsls/transforms  dsls/desired-transforms dsls/maxima
dsllinks := $(foreach l, $(dsls), $(dir_building)/src/$(l))
uilink   := $(dir_building)/src/frontend

dobuild : $(dsllinks) $(uilink)
	@cd $(dir_building)/src/grammars && ./auto-create-symlinks.sh && cd ../../..
	@cd $(dir_building) && $(MAKE) all DIR_BUILD_ROOT=$(dir_binaries) && cd ..

$(dsllinks) : $(dir_building)/src/% : %
	@ln -s ../../../$^ $@

$(uilink) : frontend
	@ln -s ../../$^ $@


### Release
# Copy the necessary binaries, scripts, configuration files and source code
# (e.g. the C++ iit-rbd library) into the release folder.
#
dir_release := release
dir_release_exe := $(dir_release)/exe
dir_release_etc := $(dir_release)/etc

run_script:= $(dir_building)/etc/robcogen.sh


release : dobuild  | release-folder
	@$(cmd_sync) $(dir_build_out)/ $(dir_release_exe)/
	@$(cmd_sync) maxima-libs  $(dir_release_exe)
	@$(cmd_sync) octave-tests $(dir_release_etc)
	@$(cmd_sync) cpp-iitrbd   $(dir_release_etc)
	@$(cmd_sync) sample       $(dir_release)
	@cp $(run_script) $(dir_release_exe)/


release-folder :
	@ mkdir -p $(dir_release_exe)

release-purge :
	rm -rf $(dir_release)/


debug :
	@ echo $(dsllinks)


.PHONY : all, dobuild, release, release-folder, release-purge, debug

